'use strict';

require('angular');
require('modules/Home');

var HomeService = function(){}

module.exports = angular
  .module('app.home')
  .service('homeService', [HomeService]);
