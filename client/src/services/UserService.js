(function(){
  'use strict';

  angular.module('app.users')
         .service('userService', ['__env', '$q', '$http', 'abmService', UserService]);

  function UserService(__env, $q, $http, abmService) {
    var self = this;
    
    this.type     = 'users';
    this.singular = 'user';
    this.IDNAME   = 'user_id';
    
    this.logged = function() {
      // Simple GET request example:
      return $q(function(resolve, reject){
        $http({
          method: 'GET',
          url: __env.apiUrl + self.type + '/logged'
        }).then(function successCallback(response) {
          return resolve(response);
        }, function errorCallback(response) {
          return reject(response);
        });
      });
    };
    
    this.get = function(id) {
      return abmService.get(self.type, id);
    };
    
    this.edit = function(id){
      abmService.edit(self.singular, id);
    };
    
    this.returnToList = function(state){
      abmService.returnToList(state || self.type);
    };
    
    this.count = function() {
      return abmService.count(self.type);
    };
    
    this.search = function(params) {
      return abmService.search(self.type, params);
    };
    
    this.save = function(params){
      return abmService.save(self.type, params, self.IDNAME);
    };
    
    this.create = function(params) {
      return abmService.create(self.type, params);
    };
    
    this.update = function(params) {
      return abmService.update(self.type, params, self.IDNAME);
    };
    
    this.delete = function(params) {
      return abmService.delete(self.type, params[self.IDNAME]);
    };
    
    this.bulkDelete = function(elements) {
      return abmService.bulkDelete(self.type, self.IDNAME, elements);
    };
    
    this.onOrderChange = function(column) {
      return abmService.onOrderChange(self.type, column);
    };
    
    this.searchByText = function(text) {
      return abmService.searchByText(self.type, text);
    };
    
    this.updateElementInArray = function(elements, element) {
      return abmService.updateElementInArray(self.IDNAME, elements, element);
    };
    
    this.removeElementsFromArray = function(elements, selected, elementsToRemove) {
      return abmService.removeElementsFromArray(self.IDNAME, elements, selected, elementsToRemove);
    };
    
    this.openDialogEdit = function(ev, mdDialog, controller, controllerAs, templateUrl, data, callback) {
      return abmService.openDialogEdit(ev, mdDialog, controller, controllerAs, this, templateUrl, data, callback);
    };
    
    this.openDialogDelete = function(ev, $mdDialog, title, content, label, callback) {
      return abmService.openDialogDelete(ev, $mdDialog, title, content, label, callback);
    };
    
    this.openDialogConfirm = function(ev, $mdDialog, title, content, label, okMessage, cancelMessage, callback) {
      return abmService.openDialogConfirm(ev, $mdDialog, title, content, label, okMessage, cancelMessage, callback);
    };
    
    this.import = function(file, success, error, progress) {
      return abmService.import(self.type, file, success, error, progress);
    };
    
    this.parseSaveResponse = function(response, parent, edit){
      abmService.parseSaveResponse(response, parent, edit);
    };
    
    this.getErrorResponse = function(response, form){
      return abmService.getErrorResponse(response, form);
    };
    
    this.toast = function(message, persist){
      abmService.toast(message, persist);
    };
    
    this.searchForAutocomplete = function(text, associationID, exclude, disabled) {
      return abmService.searchForAutocomplete(self.type, text, associationID, exclude, disabled);
    };
    
    this.getAuthObject = function(){
      return abmService.getAuthObject(self.singular);
    };
    
    this.isAuthorized = function(auth){
      return abmService.isAuthorized(auth);
    };
    
  }

})();
