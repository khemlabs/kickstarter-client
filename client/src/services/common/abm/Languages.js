var languages = {
  spanish: {
    tables: {
      Users: 'Usuarios'
    },
    inputs: {
      description : 'Descripción',
      username    : 'Nombre de usuario'
    }
  }
};

module.exports = languages;
