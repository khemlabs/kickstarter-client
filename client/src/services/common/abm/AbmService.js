'use strict';

require('angular');
require('modules/common/Abm');
require('services/common/AuthService');

function AbmService(__env, $q, $http, $state, $mdToast, authService) {

  var self = this;

  var languages = require('services/common/abm/Languages');

  this.toastElement = {
    element: {},
    persistent: false
  };

  this.metadata = function(type, id) {
    // Simple GET request example:
    return $q(function(resolve, reject){
      $http({
        method: 'GET',
        url: __env.apiUrl + 'metadata/' + type + '/' + id
      }).then(function successCallback(response) {
        return resolve(
          response.data && response.data.metadata
            ? response.data.metadata
            : {due_date: '', duration: '', budget: {total: 0, partial: 0}}
        );
      }, function errorCallback(response) {
        return reject(response);
      });
    });
  };

  this.count = function(type) {
    // Simple GET request example:
    return $q(function(resolve, reject){
      $http({
        method: 'GET',
        url: __env.apiUrl + type + '/count'
      }).then(function successCallback(response) {
        return resolve(response);
      }, function errorCallback(response) {
        return reject(response);
      });
    });
  };

  this.edit = function(type, id){
    var data = {};
    if(id) data.id = id;
    $state.transitionTo(type, data);
  };

  this.returnToList = function(type){
    $state.transitionTo(type);
  };

  this.get = function(type, id) {
    // Simple GET request example:
    return $q(function(resolve, reject){
      $http({
        method: 'GET',
        url: __env.apiUrl + type + '/' + id
      }).then(function successCallback(response) {
        if(response.data)
          return resolve(response.data);
        else return resolve(false);
      }, function errorCallback(response) {
        return reject(response);
      });
    });
  };

  this.search = function(type, params) {
    // Simple GET request example:
    return $q(function(resolve, reject){
      $http({
        method: 'GET',
        url: __env.apiUrl + type,
        params: parseParams(params),
        //headers:  {
        //  'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsInVzZXJuYW1lIjoiYWRtaW4iLCJ0eXBlIjoic3VwZXJhZG1pbiIsImlhdCI6MTQ3MjIzMTAxNywiZXhwIjoxNDcyODM1ODE3fQ.rejnQ_Yd4NMcA7_ZO7E8ibL62J5faYlJ7h8b3dvMCCY'
        //}
      }).then(function successCallback(response) {
        return resolve(response);
      }, function errorCallback(response) {
        return reject(response);
      });
    });
  };

  this.advanceSearch = function(type, params) {
    params.type = type;
    // Simple GET request example:
    return $q(function(resolve, reject){
      $http({
        method: 'GET',
        url: __env.apiUrl + 'search/advance',
        params: parseAdvanceParams(params)
      }).then(function successCallback(response) {
        return resolve(response);
      }, function errorCallback(response) {
        return reject(response);
      });
    });
  };

  this.save = function(type, params, IDNAME){
    return (params[IDNAME]) ? this.update(type, params, IDNAME) : this.create(type, params);
  };

  this.create = function(type, params) {
    return $q(function(resolve, reject){
      $http.post(__env.apiUrl + type, params).then(function successCallback(response) {
        return resolve(response);
      }, function errorCallback(response) {
        return reject(response);
      });
    });
  };

  this.update = function(type, params, IDNAME) {
    return $q(function(resolve, reject){
      $http.put(__env.apiUrl + type + '/' + params[IDNAME], params).then(function successCallback(response) {
        return resolve(response);
      }, function errorCallback(response) {
        return reject(response);
      });
    });
  };

  this.delete = function(type, id) {
    return $q(function(resolve, reject){
      $http.delete(__env.apiUrl + type + '/' + id).then(function successCallback(response) {
        return resolve(response);
      }, function errorCallback(response) {
        return reject(response);
      });
    });
  };

  this.bulkDelete = function(type, IDNAME, elements) {
    var querys = [];
    for(var idx in elements){
      querys.push($http.delete(__env.apiUrl + type + '/' + elements[idx][IDNAME]));
    }

    return $q(function(resolve, reject){
      $q.all(querys).then(function successCallback(response) {
        var elements = [];
        for(var i in response){
          var element = {
            status: response[i].status,
            statusText: response[i].statusText
          };
          element[IDNAME] = response[i].config.url.replace('https://', '').replace('http://', '').split("/")[2];
          elements.push(element);
        }
        return resolve(elements);
      }, function errorCallback(response) {
        return reject(response);
      });
    });
  };

  this.onOrderChange = function (type, data) {
    data.sort = data.sort.replace('element.', '');
    return this.search(type, data);
  };

  this.searchByText = function(type, text, column) {
    var data = {q: text};
    if(column) data.sort = column.replace('element.', '');
    return this.search(type, data);
  };

  this.searchForAutocomplete = function(type, text, associationID, exclude, disabled) {
    if(!exclude) exclude = [];
    var removeIDS = [];
    removeIDS = exclude.map(function(element){
      return element[associationID];
    });
    var data = {q: text, autocomplete: true};
    if(disabled) data.disabled = true;
    if(removeIDS.length) data.exclude = removeIDS;
    return this.search(type, data).then(function(response){
      return response.data;
    });
  };

  this.updateElementInArray = function(IDNAME, elements, element) {
    var found = false;
    for(var i in elements) {
      if(elements[i][IDNAME] == element[IDNAME]){
        for(var z in element) {
          elements[i][z] = element[z];
        }
        found = true;
        break;
      }
    }
    if(!found) elements.push(element);
    return elements;
  };

  this.removeElementsFromArray = function(IDNAME, elements, selected, elementsToRemove) {
    var removeIDS = [];
    for(var i in elementsToRemove) {
      if(elementsToRemove[i].status == 200)
        removeIDS.push( parseInt( elementsToRemove[i][IDNAME] ) );
    }
    return {
      elements: elements.filter(function(element){
        return removeIDS.indexOf(element[IDNAME]) == -1;
      }),
      selected: selected.filter(function(element){
        return removeIDS.indexOf(element[IDNAME]) == -1
      }),
    };
  };

  this.openDialogEdit = function(ev, mdDialog, controller, controllerAs, service, templateUrl, data, callback) {
    mdDialog.show({
      controller: controller,
      controllerAs: controllerAs,
      templateUrl: templateUrl,
      $scope: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose: false,
      locals: {
        abmService: service,
        data: data
      }
    })
    .then(function(element){
      callback(element);
    });
  };

  this.openDialogDelete = function(ev, $mdDialog, title, content, label, callback) {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.confirm()
        .title(title)
        .htmlContent(content)
        .ariaLabel(label)
        .targetEvent(ev)
        .ok('Sí, eliminar.')
        .cancel('No, volver al listado.');
      // Open dialog
      $mdDialog.show(confirm).then(function() {
        callback();
      });
  };

  this.openDialogConfirm = function(ev, $mdDialog, title, content, label, okMessage, cancelMessage, callback) {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.confirm()
        .title(title)
        .htmlContent(content)
        .ariaLabel(label)
        .targetEvent(ev)
        .ok(okMessage)
        .cancel(cancelMessage);
      // Open dialog
      $mdDialog.show(confirm).then(function() {
        return callback(true);
      }, function(result){
        return callback(false);
      });
  };

  this.getErrorResponse = function(response, form){
    var error = {found: false, field: false, unique: false, email: false, foreign: false, server: false, message: response};
    if(response.data && response.data.errors){
      for(var i = 0; i < response.data.errors.length; i++){
        var errorData = response.data.errors[i];
        error.field   = errorData.field ? getErrorField(errorData.field) : false;
        if(error.message){
          error.found = true;
          error.message = errorData.message ? errorData.message : errorData;
          if(error.message.indexOf("unique") >= 0){
            var msg = error.field
              ? 'El valor de (' + getSpanishInputName(error.field) + ') ya existe'
              : 'El valor ingresado ya existe';
            showToast(msg);
          } else if(error.message.indexOf("isEmail") >= 0){
            var msg = error.field
              ? 'El valor de (' + getSpanishInputName(error.field) + ') no corresponde con un email válido'
              : 'El valor ingresado no corresponde con un email válido';
            showToast(msg);
          } else if(error.message.indexOf("violates foreign key constraint") >= 0){
            var tableName = getTableName(error.message);
            showToast('El elemento no puede ser eliminado debido a que está siendo utilizado en ('+tableName+')');
          } else {
            showToast('Hubo un error en el servidor, si el error persiste comuníquese con dev@khemlabs.com');
          }
          break;
        }
      }
    }
    //return parseError(error, form);
  };

  this.parseSaveResponse = function(response, parent, edit){
    if(response.status && response.status == 200 || response.status == 201){
      if(edit && edit.data){
        edit.addForm.$setPristine();
        edit.data = response.data;
      }
      if(parent){
        parent.setElement(response.data);
      }
      showToast('El elemento fué guardado correctamente');
    } else {
      if(edit){
        self.getErrorResponse(response, edit.addForm);
      } else {
        self.getErrorResponse(response);
      }
    }
  };

  this.toast = function(message, confirm){
    return showToast(message, confirm);
  };

  this.addAssociation = function(element, association, associationName, associationID) {
    var added = false;
    if(association){
      if(!element[associationName]) element[associationName] = [];
      var actualIDS = element[associationName].map(function(element){
        return element[associationID];
      });
      if(actualIDS.indexOf(element[associationID])==-1){
        element[associationName].push(association);
        added = true;
      }
    }
    return added;
  };

  this.removeAssociations = function(element, associations, associationName, associationID) {
    var removeIDS = associations.map(function(element){
      return element[associationID];
    });
    element[associationName] = element[associationName].filter(function(element){
      return removeIDS.indexOf(element[associationID]) == -1;
    });
    return true;
  };


  /*
  {"msg":{"entity":"component","type":"not_exist","code":"prueba"}}
  */
  this.parseErrorImport = function(error, ids){
    if(error.data){
      // COLUMN NOT FOUND
      if(error.data.columnsNotFound)
        return getImportNotFound(error.data.columnsNotFound);
      // ERROR.DATA.MSG RESPONSE TYPE
      if(error.data.msg){
        // ERROR.DATA.MSG.ERROR RESPONSE TYPE
        if(error.data.msg.errors && error.data.msg.errors.length)
          return getImportErrors(error.data.msg.errors);
        // ERROR.DATA.MSG.NAME RESPONSE TYPE
        if (error.data.msg.name && error.data.msg.message)
          return getImportErrorName(error.data.msg.message);
        if (error.data.msg.entity || (error.data.msg.length && error.data.msg[0].type))
          return getImportErrorEntity(error.data.msg, ids);
      }
    }
    // UNDEFINED ERROR TYPE
    return {
      type: 'undefined',
      data: ''
    };
  };

  /**
  * Separate an array of elements on diferents arrays
  * @param {array} elements
  * @param {integer} columns
  * @param {string} title is the name of the key attribute to be display as title in the view
  */
  this.elementsInColumnsForView = function(elements, columns, title) {
    var i = 0;
    var elementsImages = [];
    elements.forEach(function(element){
      if(i==columns) i = 0;
      if(!elementsImages[i]) elementsImages[i] = [];
      var data = angular.copy(element);
      data.title = (title && data[title] && data[title].length > 13)
        ? data[title].substring(0, 11) + '..'
        : data[title];
      elementsImages[i].push(data);
      i++;
    });
    return elementsImages;
  };

  this.getAuthObject = function(type){
    type = type.charAt(0).toUpperCase() + type.slice(1);
    return {
      list      : self.isAuthorized({model: type, action: 'list'}),
      read      : self.isAuthorized({model: type, action: 'read'}),
      create    : self.isAuthorized({model: type, action: 'create'}),
      update    : self.isAuthorized({model: type, action: 'update'}),
      delete    : self.isAuthorized({model: type, action: 'delete'}),
      fulldelete: self.isAuthorized({model: type, action: 'fulldelete'})
    };
  };

  this.isAuthorized = function(auth){
    return authService.isAuthorized(auth);
  };

  var getImportNotFound = function(columns){
    return {
      type: 'not_found',
      data: columns.join()
    };
  };

  var getImportErrors = function(errors){
    if(errors[0].type && errors[0].type == 'unique violation'){
      return {
        type  : 'unique',
        column: errors[0].path  || 'columna no detectada',
        value : errors[0].value || 'columna no detectada'
      };
    } else if(errors[0].type && errors[0].type == 'Validation error'){
      return (errors[0].message && errors[0].message.indexOf('notEmpty') >= 0)
        ? {type: 'empty', data: errors[0].path || 'columna no detectada'}
        : {type: 'validation', data: errors[0].path || 'columna no detectada'};
    }
    return {
      type: 'undefined',
      data: ''
    };
  };

  var getImportErrorName = function(message){
    var init    = message.indexOf('column \"');
    var end     = '\"';
    var column  = 'columna no encontrada';
    if( init >= 0 ){
      column = message.substring(init + 8, message.length);
      column = column.substring(0, column.indexOf(end));
      return {
        type: 'validation',
        data: column
      };
    }
    return {
      type: 'undefined',
      data: ''
    };
  };

  var getImportErrorEntity = function(message, ids){
    var msg = '';
    if(!message.type && (message.length && message[0].type)){
      var dont_exist_code = [];
      var dont_exist_id   = [];
      var exist           = {};
      var existLength     = 0;
      var amount_code     = [];
      var amount_id       = [];

      message.forEach(function(errMsg){
        switch(errMsg.type){
          case 'not_exist':
            if(errMsg.code){
              dont_exist_code.push(JSON.stringify(errMsg));
            } else {
              dont_exist_id.push(errMsg);
            }
            break;
          case 'exist':
            existLength ++;
            if(!exist[errMsg.entity]) exist[errMsg.entity] = [];
            exist[errMsg.entity].push = errMsg.description;
            break;
          case 'amount':
            if(errMsg.code){
              amount_code.push(errMsg.code);
            } else {
              amount_id.push(errMsg.component_id);
            }
            break;
        }
      });

      if(ids){
        return {
          type: 'entity',
          data: {
            dont_exist_ids   : dont_exist_id,
            amount_ids       : amount_id,
            dont_exist_codes : dont_exist_code,
            amount_codes     : amount_code,
            exist_ids        : exist
          }
        }
      }

      if(dont_exist_code.length){
        msg += ' Los componentes (';
        var codes = dont_exist_code.map(function(code){
          return JSON.parse(code).code;
        });
        msg += codes.join(',') + ') no existen.';
      } else if(dont_exist_id.length){
        msg += ' Los componentes (' + dont_exist_id.join(',') + ') no existen.';
      }

      if(amount_code.length){
        msg += ' No hay suficiente cantidad de componentes con código (' + amount_code.join(',') + ').';
      } else if(amount_id.length){
        msg += ' No hay suficiente cantidad de componentes con ID (' + amount_id.join(',') + ').';
      }

      if(existLength){
        for(var key in exist){
          msg += ' La entidad (' + key + ') con descripción (' + exist[key].join(',') + ') ya existe';
        }
      }
    } else {
      if(ids){
        return {
          type: 'entity',
          data: {
            dont_exist_ids   : (message.type == 'not_exist' && message.component_id) ? [JSON.stringify(message)] : [],
            amount_ids       : (message.type == 'amount'    && message.component_id) ? [message.component_id]    : [],
            dont_exist_codes : (message.type == 'not_exist' && message.code )        ? [JSON.stringify(message)] : [],
            amount_codes     : (message.type == 'amount'    && message.code)         ? [message.code]            : [],
            exist_ids        : (message.type == 'exist')                             ? [message.component_id]    : [],
          }
        }
      }
      switch(message.type){
        case 'not_exist':
          msg = message.code
            ? 'El componente (' + message.code + ') no existe'
            : 'El componente con ID (' + message.component_id || 'sin definir' + ') no existe';
          break;
        case 'exist':
          msg = 'La entidad (' + message.entity + ') con descripción (' + message.description + ') ya existe';
          break;
        case 'amount':
          msg = 'No hay suficiente cantidad de componentes con código (' + (message.code||message.component_id) + ')';
          break;
      }
    }
    return {
      type: 'entity',
      data: msg
    }
  };

  var getTableName = function(error) {
    var pos = error.indexOf("violates foreign key constraint");
    var tableName = error.substring(pos, error.length);
    var tablePos = tableName.indexOf("on table");
    if(tablePos >= 0){
      tablePos = tablePos + 10;
      tableName = tableName.substring(tablePos, error.length);
      var endColumnName = tableName.indexOf('"');
      tableName = tableName.substring(0, endColumnName);
      return getSpanishTableName(tableName);
    }
  };

  var getSpanishTableName = function(tableName) {
    if(languages.spanish && languages.spanish.tables[tableName]){
      return languages.spanish.tables[tableName];
    }
    console.log('Table not found in TablesInSpanish');
    return tableName;
  };

  var getSpanishInputName = function(inputName) {
    if(languages.spanish && languages.spanish.inputs[inputName]){
      return languages.spanish.inputs[inputName];
    }
    console.log('Input not found in InputInSpanish');
    return inputName;
  };

  var showToast = function(message, persist) {
    if(self.toastElement.persistent){
      console.log('Message: ' + message + ' was not toested, persistent toast open');
    } else {
      self.toastElement.element = $mdToast.simple()
        .textContent(message)
        .action('X')
        .highlightAction(true)
        .position('top center')
        .hideDelay(persist ? false : 5000);
      self.toastElement.persistent = persist || false;
      $mdToast.show(self.toastElement.element).then(function(){
        self.toastElement.element = {};
        self.toastElement.persistent = false;
      });
    }
  };

  var parseError = function(error, form){
    if(error.found && error.field && form[error.field]){
      form[error.field].$error.unique = error.unique;
      form[error.field].$error.email  = error.email;
      form[error.field].$error.server = error.server;
    }
  };

  var getErrorField = function(field) {
    //"lower(COLUMN_NAME::text)"
    if(field){
      if(field.indexOf("lower") >= 0 && field.indexOf("::") > 7){
        var finalColumn = field.indexOf("::");
        field = field.substring(6, finalColumn);
      }
    } else {
      field = false;
    }
    return field;
  };

  var parseParams = function(params) {
    if(!params) var params = {};
    if(!params.sort) params.sort = self.defaultSort;
    if(!params.count && params.count !== 'all'){
      params.count = 100;
    } else if (params.count == 'all') {
      delete params.count;
    }

    return params;
  };

  var parseAdvanceParams = function(params) {
    if(params.searchData){
      for(var key in params.searchData){
        params[key] = params.searchData[key];
      };
      delete params.searchData;
    }
    return parseParams(params);
  };
}

var abmService = angular
  .module('app.common.abm')
  .service('abmService', ['__env', '$q', '$http', '$state', '$mdToast', 'authService', AbmService]);
module.exports = abmService;
