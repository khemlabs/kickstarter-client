'use strict';

require('angular');
require('angular-storage');

require('modules/common/Login');

function loginService(__env, $q, $http, store, auth){

  var self = this;

  this.loginURI = __env.apiUrl + 'users/login';

  this.login = function(data) {
    var defer = $q.defer();

    $http.post(self.loginURI, data).then(function(res){
      self._saveData(res.data);
      return defer.resolve(res.data);
    }, function(err){
      return defer.reject(err);
    });

    return defer.promise;
  };

  this.isAuthenticated = function(){
    return auth.isAuthenticated();
  };

  this._saveData = function(data) {
    auth.setSession(data);
  };

}

module.exports = angular
  .module('app.common.login')
  .service('loginService', ['__env', '$q', '$http', 'store', 'authService', loginService]);

// The inclusion of circular dependencies must be performed after "module.exports"
require('services/common/AuthService');
