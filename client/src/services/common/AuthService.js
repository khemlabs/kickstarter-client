'use strict';

require('angular');
require('angular-storage');

require('modules/common/Auth');

//function AuthService(__env, $q, $http, $rootScope, $state, store, jwtHelper, socketFactory){
function AuthService(__env, $q, $http, $rootScope, $state, store, jwtHelper){

  var self = this;

  //self.socket = null;

  this.$stateChangeStart = function(){
    $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
      if (!self.__accessGranted(toState.authenticate, toState.validTypes)){
        event.preventDefault();
        if(!self.isAuthenticated()){
          store.set('redirect', toState.name);
          if(toParams) store.set('params', toParams);
          $state.transitionTo("login");
        } else {
          $state.transitionTo("unauthorized");
        }
      }
      // Uncomment => npm install coksrt 
      //if(!self.socket && toState.authenticate) self.__initSocket();
    });
  };

  this.__accessGranted = function(authenticate, types){
    if(!authenticate) return true;
    if(!types) return self.isAuthenticated();
    return self.isAuthorized(types);
  };

  this.__initSocket = function(){
    /*
    self.socket = socketFactory({
      ioSocket: io.connect(__env.apiUrl, {
        query: 'token=' + store.get('jwt')
      })
    });

    self.socket.on('connect', function(data){
      console.log('User connected.');
    });
    */
  };

  this.isAuthenticated = function(){
    return store.get('jwt') && !jwtHelper.isTokenExpired(store.get('jwt'));
  };

  this.isAuthorized = function(auth){
    if(self.isAuthenticated() && auth){
      if(auth.model && auth.action){
        var rules = store.get('rules');
        return (rules[auth.model] && rules[auth.model][auth.action]);
      }
      return auth.indexOf(store.get('usertype')) >= 0;
    }
    return false;
  };

  this.setSession = function(data){
    store.set('jwt',         data.token);
    store.set('username',    data.username);
    store.set('first_name',  data.first_name);
    store.set('last_name',   data.last_name);
    store.set('user_status', data.status);
    store.set('userarea',    data.area);
    store.set('usertype',    data.type);
    store.set('rules',       data.rules || {});
  };

  this.disconnect = function(){
    store.remove('jwt');
    store.remove('username');
    store.remove('first_name');
    store.remove('last_name');
    store.remove('user_status');
    store.remove('userarea');
    store.remove('usertype');
    store.remove('rules');
    //self.socket.disconnect();
    //self.socket = null;
    //console.log('Websocket disconnected');
  };
}

module.exports = angular
  .module('app.common.auth')
  .service('authService', ['__env', '$q', '$http', '$rootScope', '$state', 'store', 'jwtHelper', AuthService]);
  //.service('authService', ['__env', '$q', '$http', '$rootScope', '$state', 'store', 'jwtHelper', 'socketFactory', AuthService]);
