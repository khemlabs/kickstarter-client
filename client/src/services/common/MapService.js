// WARNING: npm install not available
'use strict';

require('angular');
require('modules/common/Maps');

function MapService( $q, NgMap ) {
  var self = this;

  this.lib = NgMap;

  this.addMarker = function(map, data){
    if(!map.custommarkers) map.custommarkers = [];

    if(!data.geometry){
      data.geometry = {
        location: {
          lat: parseFloat(data.lat),
          lng: parseFloat(data.lng)
        }
      };
    }

    var icon = {
      url         : location.protocol + "//" + location.host + '/assets/images/icons/geocode-71.png',
      size        : new google.maps.Size(71, 71),
      origin      : new google.maps.Point(0, 0),
      anchor      : new google.maps.Point(17, 34),
      scaledSize  : new google.maps.Size(25, 25)
    };

    // Create a marker for each place.
    var markerbuild = new google.maps.Marker({
      map       : map,
      icon      : icon,
      title     : data.name,
      position  : data.geometry.location
    });
    map.custommarkers.push(markerbuild);

    // For each marker, get the icon, name and location.
    var bounds = new google.maps.LatLngBounds();
    bounds.extend(markerbuild.position);
    map.fitBounds(bounds);

    return markerbuild;
  };

  /*
  * Add an input for autocomplete search
  * @param {string} [optional] map
  * @param {object} config {id: {string}, [position: {map.ControlPosition}], inMapViewport: {boolean}}
  * @param {function} callback this is called when search input changed
  */
  this.addSearchInput = function(map, config, callback){
    return $q(function(resolve, reject){
      // Create the search box and link it to the UI element.
      var input = self.__loadSearchBox(map, config.id);
      self.__loadSearchBoxOnChange(map, callback);
      self.__loadSearchBoxPlacesChanged(map);
    });
  };

  // ---------- Private methods ----------

  this.__loadSearchBox = function(map, id){
    var input = document.getElementById(id);
    map.searchBox = new google.maps.places.SearchBox(input);
    return input;
  };

  this.__loadSearchBoxOnChange = function(map, callback){
    map.searchBox.onChange = function(){
      return callback(map.places);
    };
  };

  this.__loadSearchBoxPlacesChanged = function(map){
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    map.searchBox.addListener('places_changed', function() {
      var places = map.searchBox.getPlaces();
      if (places.length == 0) {
        return;
      }
      // Clear out the old markers.
      self.__resetMarkers(map, places);
      map.searchBox.onChange();
    });
  };

  this.__resetMarkers = function(map, places){
    if(!map.custommarkers) map.custommarkers = [];
    // remove old places
    self.__removeMarkers(map);
    // add new places
    map.places = self.__addMarkers(map, places);
  };

  this.__removeMarkers = function(map, markers){
    // Foreach marker find him in the array ok markers to be deleted
    map.custommarkers.forEach(function(marker){
      marker.setMap(null);
    });
    map.custommarkers = [];
  };

  this.__addMarkers = function(map, data){
    var markersbuild = [];
     data.forEach(function(marker){
       markersbuild.push(self.addMarker(map, marker));
     });
    return markersbuild;
  };
}

module.exports = angular
  .module('app.common.maps')
  .service('mapService', [ '$q', 'NgMap', MapService ]);
