'use strict';

require('modules/common/Files');

function FilesService( __env, Upload ) {
  var self = this;

  this.import = function(type, file, success, error, progress) {
    return Upload.upload({
      url: __env.apiUrl +'imports/' + type,
        data: {import: file}
    }).then(function (resp) {
      success(resp.data || resp);
    }, function (resp) {
      error(resp);
    }, function (evt) {
      if(progress){
        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        progress(progressPercentage);
      }
    });
  };
  
}

module.exports = angular.module('app.common.files')
  .service('filesService', ['__env', 'Upload', FilesService]);
