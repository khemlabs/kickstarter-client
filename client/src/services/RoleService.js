'use strict';

require('angular');
require('modules/Role');

function RoleService($q, $http, abmService) {
  var self = this;

  this.type     = 'roles';
  this.singular = 'role';
  this.IDNAME   = 'role_id';

  this.metadata = function(id) {
    return abmService.metadata(self.type, id);
  };

  this.count = function() {
    return abmService.count(self.type);
  };

  this.edit = function(id){
    abmService.edit(self.singular, id);
  };

  this.returnToList = function(state){
    abmService.returnToList(state || self.type);
  };

  this.get = function(id) {
    return abmService.get(self.type, id);
  };

  this.search = function(params) {
    return abmService.search(self.type, params);
  };

  this.save = function(params){
    return abmService.save(self.type, params, self.IDNAME);
  };

  this.create = function(params) {
    return abmService.create(self.type, params);
  };

  this.update = function(params) {
    return abmService.update(self.type, params, self.IDNAME);
  };

  this.delete = function(params) {
    return abmService.delete(self.type, params[self.IDNAME]);
  };

  this.bulkDelete = function(elements) {
    return abmService.bulkDelete(self.type, self.IDNAME, elements);
  };

  this.onOrderChange = function(column) {
    return abmService.onOrderChange(self.type, column);
  };

  this.searchByText = function(text) {
    return abmService.searchByText(self.type, text);
  };

  this.updateElementInArray = function(elements, element) {
    return abmService.updateElementInArray(self.IDNAME, elements, element);
  };

  this.removeElementsFromArray = function(elements, selected, elementsToRemove) {
    return abmService.removeElementsFromArray(self.IDNAME, elements, selected, elementsToRemove);
  };

  this.openDialogEdit = function(ev, mdDialog, controller, controllerAs, templateUrl, data, callback) {
    return abmService.openDialogEdit(ev, mdDialog, controller, controllerAs, this, templateUrl, data, callback);
  };

  this.openDialogDelete = function(ev, $mdDialog, title, content, label, callback) {
    return abmService.openDialogDelete(ev, $mdDialog, title, content, label, callback);
  };

  this.openDialogConfirm = function(ev, $mdDialog, title, content, label, okMessage, cancelMessage, callback) {
    return abmService.openDialogConfirm(ev, $mdDialog, title, content, label, okMessage, cancelMessage, callback);
  };

  this.import = function(file, success, error) {
    return abmService.import(self.type, file, success, error);
  };

  this.parseSaveResponse = function(response, parent, edit){
    abmService.parseSaveResponse(response, parent, edit);
  };

  this.getErrorResponse = function(response, form){
    return abmService.getErrorResponse(response, form);
  };

  this.toast = function(message, persist){
    abmService.toast(message, persist);
  };

  this.searchForAutocomplete = function(text, associationID, exclude, disabled) {
    return abmService.searchForAutocomplete(self.type, text, associationID, exclude, disabled);
  };

  this.addAssociation = function(element, associations, associationName, associationID) {
    return abmService.addAssociation(element, associations, associationName, associationID);
  };

  this.removeAssociations = function(element, associations, associationName, associationID) {
    return abmService.removeAssociations(element, associations, associationName, associationID);
  };

  this.parseErrorImport = function(error){
    return abmService.parseErrorImport(error);
  };

  /**
  * Separate an array of elements on diferents arrays
  * @param {array} elements
  * @param {integer} columns
  * @param {string} title is the name of the key attribute to be display as title in the view
  */
  this.elementsInColumnsForView = function(elements, columns, title) {
    return abmService.elementsInColumnsForView(elements, columns, title);
  };

  this.getAuthObject = function(){
    return abmService.getAuthObject(self.singular);
  };

  this.isAuthorized = function(auth){
    return abmService.isAuthorized(auth);
  };

}

module.exports = angular
  .module('app.roles')
  .service('roleService', ['$q', '$http', 'abmService', RoleService]);

// The inclusion of circular dependencies must be performed after "module.exports"
require('services/common/abm/AbmService');
