'use strict';

// Compile sass
require("stylesheets/main.scss");

// Require dependencies
require('angular');
require('angular-material');
require('angular-sanitize');
require('angular-ui-router');
require('angular-storage');
require('angular-jwt');
require('angular-material-data-table');
// Uncomment for imports
//require('ng-file-upload');
//require('ng-csv');
// Uncomment for sockets
//require('angular-socket-io');
// Uncomment for maps - WARNING: npm install not available
//require('ngmap');

var dependencies = [
  'ngMaterial'            ,
  'ngSanitize'            ,
  'ngAnimate'             ,
  'ui.router'             ,
  'angular-storage'       ,
  'angular-jwt'           ,
  'md.data.table'         ,
  // Uncomment for imports
  //'ngFileUpload'          ,
  //'ngCsv'                 ,
  // Uncomment for sockets
  //'btford.socket-io'      ,
  // Uncomment for maps - WARNING: npm install not available
  //'ngMap'                 ,
];
var common = [
  'app.common.abm'        ,
  'app.common.auth'       ,
  'app.common.directives' ,
  'app.common.env'        ,
  'app.common.example'    ,
  'app.common.factories'  ,
  'app.common.login'      ,
  'app.common.menu'       ,
  // Uncomment for imports
  //'app.common.files'      ,
  //'app.common.filters'    ,
  
  // Uncomment for maps - WARNING: npm install not available
  //'app.common.maps'       ,
];
var appModules = [
  'app.administrators',
  'app.home',
  'app.roles',
  'app.users'
];

// Init app
const app = angular.module('app', [].concat(dependencies, common, appModules));

// Run app
app.run([
  '$rootScope',
  '$mdComponentRegistry',
  '$state',
  'store',
  'authService',
  require('modules/common/Run')
]);

// Export app
module.exports = app;

// Apply config
require('config');