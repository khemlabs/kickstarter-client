'use strict';

require('modules/common/Filters');

function dateFormater() {
  return function(unformattedDate) {
    return DateWithoutTime(unformattedDate);
  }
}

module.exports = angular
  .module('app.common.filters')
  .factory('dateFormater', [dateFormater]);
