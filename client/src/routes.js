'use strict';

// Routes definition
var routes = [
  {
    state : 'example',
    config: {
      url         : "/example"                              ,
      templateUrl : "/templates/common/example/example.html",
      controller  : "ExampleController"                     ,
      controllerAs: "example"                               ,
      authenticate: false                                   ,
      validTypes  : []
    }
  },
  {
    state: 'users',
    config: {
      url: "/users",
      templateUrl : "/templates/users/users.html",
      controller  : "UserController",
      controllerAs: "user",
      authenticate: true,
      validTypes  : ['superadmin', 'admin']
    }
  },
  {
    state: 'user',
    config: {
      url: "/user/:id",
      templateUrl : "/templates/users/edit.html",
      controller  : "EditUserController",
      controllerAs: "edituser",
      authenticate: true,
      validTypes  : ['superadmin', 'admin']
    }
  },
  {
    state: 'administrators',
    config: {
      url: "/administrators",
      templateUrl : "/templates/administrators/administrators.html",
      controller  : "AdministratorController",
      controllerAs: "administrator",
      authenticate: true,
      validTypes  : ['superadmin', 'admin']
    }
  },
  {
    state: 'administrator',
    config: {
      url: "/administrator/:id",
      templateUrl : "/templates/administrators/edit.html",
      controller  : "EditAdministratorController",
      controllerAs: "editadministrator",
      authenticate: true,
      validTypes  : ['superadmin', 'admin']
    }
  },
  {
    state: 'roles',
    config: {
      url: "/roles",
      templateUrl : "/templates/roles/roles.html",
      controller  : "RoleController",
      controllerAs: "role",
      authenticate: true,
      validTypes  : ['superadmin', 'admin']
    }
  },
  {
    state: 'role',
    config: {
      url: "/role/:id",
      templateUrl : "/templates/roles/edit.html",
      controller  : "EditRoleController",
      controllerAs: "editrole",
      authenticate: true,
      validTypes  : ['superadmin', 'admin']
    }
  },
  {
    state: 'assemblies',
    config: {
      url: "/assemblies",
      templateUrl : "/templates/assemblies/assemblies.html",
      controller  : "AssemblyController",
      controllerAs: "assembly",
      authenticate: true,
      validTypes  : ['superadmin', 'admin']
    }
  },
  {
    state: 'assembly',
    config: {
      url: "/assembly/:id",
      templateUrl : "/templates/assemblies/edit.html",
      controller  : "EditAssemblyController",
      controllerAs: "editassembly",
      authenticate: true,
      validTypes  : ['superadmin', 'admin']
    }
  },
  {
    state: 'subassemblies',
    config: {
      url: "/subassemblies",
      templateUrl : "/templates/subassemblies/subassemblies.html",
      controller  : "SubassemblyController",
      controllerAs: "subassembly",
      authenticate: true,
      validTypes  : ['superadmin', 'admin']
    }
  },
  {
    state: 'subassembly',
    config: {
      url: "/subassemblies/:id",
      templateUrl : "/templates/subassemblies/edit.html",
      controller  : "EditSubassemblyController",
      controllerAs: "editsubassembly",
      authenticate: true,
      validTypes  : ['superadmin', 'admin']
    }
  }
];

// Export routes
module.exports = routes;
