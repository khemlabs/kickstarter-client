'use strict';

require('modules/common/Directives');

module.exports = angular
  .module('app.common.directives')
  .directive('focusOn', function () {
    return function(scope, elem, attr) {
      scope.$on('focusOn', function(e, name) {
        if(name === attr.focusOn) {
          elem[0].focus();
        }
      });
    };
  });
