'use strict';

require('modules/common/Directives');

module.exports = angular
  .module('app.common.directives')
  .directive('ngLowercase', function () {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var lower = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var lowered = inputValue.toLowerCase();
          if (lowered !== inputValue) {
            modelCtrl.$setViewValue(lowered);
            modelCtrl.$render();
          }
          return lowered;
        }
        modelCtrl.$parsers.push(lower);
        lower(scope[attrs.ngModel]); // capitalize initial value
      }
    };
  });
