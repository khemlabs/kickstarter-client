'use strict';

require('modules/common/Directives');

module.exports = angular
  .module('app.common.directives')
  .directive('ngEnter', function() {
    return function(scope, element, attrs) {
      element.bind("keydown keypress", function(event) {
        if(event.which === 13) {
          scope.$apply(function(){
            scope.$eval(attrs.ngEnter);
          });
        }
      });
    }
  });
