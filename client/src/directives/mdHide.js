'use strict';

require('modules/common/Directives');

module.exports = angular
  .module('app.common.directives')
  .directive('mdHide', function () {
    return function(scope, elem, attr) {
      scope.$on('mdHide', function(e, name) {
        if(name === attr.focusOn) {
          elem[0].hide();
        }
      });
    };
  });
