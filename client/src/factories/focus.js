'use strict';

require('modules/common/Factories');

module.exports = angular.module('app.common.factories')
  .factory('focus', function ($timeout, $rootScope) {
    return function(name) {
      $timeout(function (){
        $rootScope.$broadcast('focusOn', name);
      });
    }
  });
