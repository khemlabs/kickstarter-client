'use strict';

require('angular');
require('directives/ngEnter');
require('modules/Role');
require('services/RoleService.js');

function RoleController( $q, $mdDialog, model ) {
  var self = this;

  this.title = 'Reglas';
  this.elements = [];
  this.selected = [];
  this.searchData = {};

  this.auth = model.getAuthObject();

  this.edit = function(id) {
    model.edit(id);
  };

  this.onOrderChange = function (column) {
    self.deferred = model.onOrderChange(column).then(function(response){
      self.elements = response.data.map(function(element){


        return element;
      });
    });
  };

  this.searchByText = function() {
    this.deferred = model.searchByText(this.searchData.text).then(function(response){
      self.elements = response.data.map(function(element){


        return element;
      });
    });
  };

  /**
  * This function allows extra param disabled.
  *
  * @associationID
  * @exclude array of objects to exlude of search
  * @disabled boolean that enables "disabled results"
  *
  * @warning: exclude and disabled need middleware implementation on server to work.
  *
  * @example: server/middlewares/Example.middleware.js in list.start
  */
  this.searchForAutocomplete = function(associationID, exclude) {
    return $q(function(resolve, reject){
      if(self.lastSearch != self.searchData.text){
        self.lastSearch = self.searchData.text;
        // If you want to recive disabled param, pass it to this function
        return model.searchForAutocomplete(self.searchData.text, associationID, exclude).then(function(data){
          self.searchCache = data;
          return resolve(self.searchCache);
        });
      } else {
        return resolve(self.searchCache);
      }
    });
  };

  this.clearAutocomplete = function() {
    self.lastSearch   = null;
    self.searchCache  = [];
  };

  this.search = function(query) {
    self.deferred = model.search(query).then(function(response){
      self.elements = response.data.map(function(element){


        return element;
      });
    });
  };

  this.delete = function(ev) {
    var title = self.selected.length > 1
        ? '¿Esta seguro que quiere eliminar los siguientes roles?'
        : '¿Esta seguro que quiere eliminar al siguiente role?';
    var label = 'Eliminación de roles';
    var content = '';
    for(var idx in self.selected){
      content += '<br>' + self.selected[idx].name;
    }
    model.openDialogDelete(ev, $mdDialog, title, content, label,
      function(){
        self.deferred = model.bulkDelete(self.selected).then(function(elements) {
          var data = model.removeElementsFromArray(self.elements, self.selected, elements);
          self.elements = data.elements;
          self.selected = data.selected;
        }, function(err){
          console.log(err);
          model.getErrorResponse(err);
        });
    });
  };

  this.search();
}

module.exports = angular
  .module('app.roles')
  .controller('RoleController', [
    '$q',
    '$mdDialog',
    'roleService',
    RoleController
  ]);
