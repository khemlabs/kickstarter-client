'use strict';

require('angular');
require('modules/Role');
require('services/RoleService.js');

function EditRoleController( focus, $params, model ) {
  var self = this;

  focus('EditRoleController');

  this.loading = true;

  this.fulldelete = [];

  this.actions = {
    list        : 'Lectura',
    create      : 'Creación',
    update      : 'Modificación',
    delete      : 'Eliminación',
    fulldelete  : 'Eliminación total'
  };
  
  this.actionskeys = Object.keys(self.actions);

  this.entities = ['User'];

  this.loadRoles = function(){
    self.entities.forEach(function(entity){
      if(!self.data.rules[entity]) self.data.rules[entity] = {};
      if(self.fulldelete.indexOf(entity) >= 0 && !self.data.rules[entity].fulldelete)
        self.data.rules[entity].fulldelete = false;
      self.actionskeys.forEach(function(key){
        if(!self.data.rules[entity][key]) self.data.rules[entity][key] = false;
        if(key == 'list') self.data.rules[entity].read = self.data.rules[entity][key];
      });
    });
  };

  this.resetParams = function(element){

    self.data = element || {
      name: '',
      rules: {}
    };

    // Build roles object
    self.loadRoles();

    self.roles = [{
      title: 'Usuarios',
      name: 'User',
      actions: [
        {
          name: 'list',
          rules: {
            uncheck: [
              {type: 'User', actions: ['create', 'update', 'delete']}
            ]
          }
        }, {
          name: 'create' ,
          rules: {
            check: [
              {type: 'User', actions: ['list']}
            ]
          }
        }, {
          name: 'update' ,
          rules: {
            check: [
              {type: 'User', actions: ['list']}
            ]
          }
        }, {
          name: 'delete' ,
          rules: {
            check: [
              {type: 'User', actions: ['list']}
            ]
          }
        },
      ]
    }];

    self.newInput = element ? false : true;
    self.loading  = false;

  };

  this.update = function(model, data){
    model       = model || false;
    var check   = data && data.check   ? data.check   : false;
    var uncheck = data && data.uncheck ? data.uncheck : false;
    if(model){
      if(check && check.length){
        check.forEach(function(el){
          if(!self.data.rules[el.type])
            self.data.rules[el.type] = {};
          if(el.actions && el.actions.length){
            el.actions.forEach(function(action){
              if(!self.data.rules[el.type][action])
                self.data.rules[el.type][action] = true;
            });
          }
        });
      }
    } else {
      if(uncheck && uncheck.length){
        uncheck.forEach(function(el){
          if(self.data.rules[el.type] && el.actions && el.actions.length){
            el.actions.forEach(function(action){
              if(self.data.rules[el.type][action])
                self.data.rules[el.type][action] = false;
            });
          }
        });
      }
    }
  };

  this.save = function(params) {
    if(self.addForm.$valid){
      self.loadRoles(); // We call loadroles to load (read action) with (list action) value
      model.save(params).then(function(response){
        model.parseSaveResponse(response, false, self);
        model.returnToList();
      }, function(response){
        model.getErrorResponse(response, self.addForm);
      });
    }
  };

  this.loadElement = function(){
    if(!$params.id){
      self.resetParams();
    } else {
     model.get($params.id).then(function(element){
        self.resetParams(element);
      }, function(err){
        model.returnToList();
      });
    }
  };

  this.loadElement();

}

module.exports = angular
  .module('app.roles')
  .controller('EditRoleController', [
    'focus',
    '$stateParams',
    'roleService',
    EditRoleController
  ]);
