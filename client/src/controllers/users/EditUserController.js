'use strict';

require('angular');
require('factories/focus');
require('modules/Users');
require('services/UserService');
require('services/RoleService');

function EditUserController( focus, $params, roles, model ) {
  var self = this;

  focus('EditUserController');

  this.loading = true;

  this.resetParams = function(element){
    self.data = element || {area: 'administration', status: 'isactive'};
    if(!self.data.user_id){
      self.editPassword = true;
    }
    self.loading = false;
  };

  this.passwordChange = function(){
    self.samePassword('password');
  };

  this.passwordConfChange = function(){
    self.samePassword('passwordConfirm');
  };

  this.samePassword = function (field) {
    self.addForm[field].$error.equal = (self.data.passwordConfirm !== self.data.password)
       ? true : false;
    if(!self.addForm[field].$error.equal){
      self.addForm['password'].$error.equal = false;
      self.addForm['password'].$invalid = false;
      self.addForm['password'].$valid = true;
      self.addForm['passwordConfirm'].$error.equal = false;
      self.addForm['passwordConfirm'].$invalid = false;
      self.addForm['passwordConfirm'].$valid = true;
    }
    self.addForm.$valid = self.addForm.$valid && !self.addForm[field].$error.equal ? true : false;
  }

  this.save = function(params) {
    if(self.addForm.$valid){
      model.save(params).then(function(response){
        model.parseSaveResponse(response, false, self);
        model.returnToList();
      }, function(response){
        model.getErrorResponse(response, self.addForm);
      });
    }
  };

  this.loadElement = function(){
    roles.search().then(function(roles){
      self.types = roles.data.map(function(rol){
        return rol.name;
      }).filter(function(rol){
        return rol != 'superadmin' && rol != 'admin';
      });
      if(!$params.id){
        self.resetParams();
      } else {
       model.get($params.id).then(function(element){
          self.resetParams(element);
        }, function(err){
          model.returnToList();
        });
      }
    });
  };

  this.loadElement();
}

module.exports = angular
  .module('app.users')
  .controller('EditUserController', [
    'focus',
    '$stateParams',
    'roleService',
    'userService',
    EditUserController
  ]);
