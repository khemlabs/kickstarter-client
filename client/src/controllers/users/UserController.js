'use strict';

require('angular');
require('directives/ngEnter');
require('modules/Users');
require('services/UserService.js');

function UserController( $mdDialog, model ) {
  var self = this;

  this.title = 'Usuarios';
  this.elements = [];
  this.selected = [];
  this.searchData = {};

  this.auth = model.getAuthObject();

  this.edit = function(id) {
    model.edit(id);
  };

  this.onOrderChange = function (column) {
    self.searchData.sort = column;
    self.deferred = model.onOrderChange(self.searchData).then(function(response){
      self.elements = response.data;
    });
  };

  this.searchByText = function() {
    this.deferred = model.searchByText(this.searchData.text).then(function(response){
      self.elements = response.data.filter(function(user){
        return user.type !== 'admin' && user.type !== 'superadmin';
      });
    });
  };

  this.search = function(query) {
    self.deferred = model.search(query).then(function(response){
      self.elements = response.data.filter(function(user){
        return user.type !== 'admin' && user.type !== 'superadmin';
      });
    });
  };

  this.reorder = function(column) {
    self.searchData.sort = column;
    self.search(self.searchData);
  };
  
  this.delete = function(ev) {
    var title = self.selected.length > 1
        ? '¿Esta seguro que quiere eliminar los siguientes usuarios?'
        : '¿Esta seguro que quiere eliminar al siguiente usuario?';
    var label = 'Eliminación de usuarios';
    var content = '';
    for(var idx in self.selected){
      content += '<br>' + self.selected[idx].username;
    }
    model.openDialogDelete(ev, $mdDialog, title, content, label,
      function(){
        self.deferred = model.bulkDelete(self.selected).then(function(elements) {
          var data = model.removeElementsFromArray(self.elements, self.selected, elements);
          self.elements = data.elements;
          self.selected = data.selected;
        }, function(err){
          console.log(err);
          model.getErrorResponse(err);
        });
    });
  };
  
  this.search();
}

module.exports = angular
  .module('app.users')
  .controller('UserController', [
    '$mdDialog',
    'userService',
    UserController
  ]);
