'use strict';

require('angular');
require('modules/common/Login');
require('services/common/LoginService.js');
var test = require('services/common/AuthService.js');

function LogoutController( $state, auth ) {

  var self = this;

  this.logout = function(){
    auth.disconnect();
    $state.transitionTo('root');
  }

  this.logout();
}

module.exports = angular
  .module('app.common.login')
  .controller('LogoutController', [
    '$state',
    'authService',
    LogoutController
  ]);
