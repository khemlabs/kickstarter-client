'use strict';

require('angular');
require('angular-storage');

require('modules/common/Login');
require('services/common/LoginService');
require('services/common/abm/AbmService');

function LoginController( loginService, abmService, store, $state ) {

  var self = this;

  this.loginData = {};
  this.loaded    = false;

  this.send = function(){
    loginService.login(self.loginData).then(
      function(data){
        return self.redirect(data.type);
      },
      function(error){
        abmService.toast('El nombre de usuario o la clave es inválida');
      }
    );
  };

  this.redirect = function(type){
    var from = store.get('redirect');
    var params = store.get('params') || {};
    if(from){
      store.set('redirect', false);
      store.set('params', false);
    } else {
      from = 'home';
      switch (type){
        case 'admin':
        case 'superadmin':
          from = 'home';
          break;
        default:
          from = 'home';
          break;
      }
    }
    $state.transitionTo(from, params);
  };

  if(loginService.isAuthenticated()){
    return self.redirect(store.get('usertype'));
  }

}

module.exports = angular
  .module('app.common.login')
  .controller('LoginController', [
    'loginService',
    'abmService',
    'store',
    '$state',
    LoginController
  ]);
