'use strict';

require('angular');
require('modules/common/Example');

function ExampleController() {
  this.title = 'Kickstarter example';
}

module.exports = angular
  .module('app.common.example')
  .controller('ExampleController', [ExampleController]);
