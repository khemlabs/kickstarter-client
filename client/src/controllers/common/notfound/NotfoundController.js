'use strict';

require('angular');

function NotfoundController( ) {}

module.exports = angular
  .module('app.common.login')
  .controller('NotfoundController', [
    NotfoundController
  ]);
