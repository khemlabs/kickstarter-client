'use strict';

require('angular');
require('angular-storage');

require('modules/common/Menu.js');

function MenuController(
  $scope,
  $timeout,
  $mdComponentRegistry,
  $mdSidenav,
  store
){
  var self = this;

  var customMenu = store.get('menu') || [];

  this.auth = [
    {alias: 'home',           name: 'Página principal', show: true},
    {alias: 'administrators', name: 'Administradores',  auth: ['superadmin']},
    {alias: 'roles',          name: 'Roles',            auth: ['superadmin']},
    {alias: 'users',          name: 'Usuarios',         model: 'User',        auth: ['superadmin']}
  ];

  // Load all custom menus
  customMenu.forEach(function(menu){
    self.auth.push(menu);
  });

  this.openPanelMenu = buildDelayedToggler('panel-menu');
  this.searchPanelMenu = buildDelayedToggler('search-menu');

  this.close = function (id) {
    self.userType = store.get('usertype');
    if(self.userType && $mdComponentRegistry.get(id || 'panel-menu')){
      var menu = $mdSidenav(id || 'panel-menu');
      if(menu){
        menu.close();
      }
    }
  };

  /**
  * Supplies a function that will continue to operate until the
  * time is up.
  */
  function debounce(func, wait, context) {
    var timer;
    return function debounced() {
      var context = $scope,
      args = Array.prototype.slice.call(arguments);
      $timeout.cancel(timer);
      timer = $timeout(function() {
        timer = undefined;
        func.apply(context, args);
      }, wait || 10);
    };
  }

  /**
  * Build handler to open/close a SideNav; when animation finishes
  * report completion in console
  */
  function buildDelayedToggler(navID) {
    return debounce(function() {
      $mdSidenav(navID)
      .toggle()
      .then(function () {
        self.loaded = true;
      });
    }, 200);
  }

  function buildToggler(navID) {
    return function() {
      $mdSidenav(navID).toggle();
    }
  }

  // Get user data and permissions
  this.init = function(){
    self.userType = store.get('usertype');
    self.rules = store.get('rules');
    if(self.userType || self.rules){
      self.auth.forEach(function(menu){
        if(menu.show !== true && menu.show !== false){
          if(self.userType && menu.auth && menu.auth.length){
            menu.show = menu.auth.indexOf(self.userType) >= 0;
          } else if (self.rules && menu.model && menu.action) {
            menu.show = self.rules[menu.model] && self.rules[menu.model][menu.action];
          }
        }
      });
      self.openPanelMenu = buildDelayedToggler('panel-menu');
      self.searchPanelMenu = buildDelayedToggler('search-menu');
      self.close();
    }
  };

  self.init();
}

module.exports = angular
  .module('app.common.menu')
  .controller('MenuController', [
    '$scope',
    '$timeout',
    '$mdComponentRegistry',
    '$mdSidenav',
    'store',
    MenuController
  ]);
