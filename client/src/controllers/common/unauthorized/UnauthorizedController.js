'use strict';

require('angular');

function UnauthorizedController( ) {}

module.exports = angular
  .module('app.common.login')
  .controller('UnauthorizedController', [
    UnauthorizedController
  ]);
