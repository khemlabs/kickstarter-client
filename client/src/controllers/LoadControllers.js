// Common controllers
require('controllers/common/example/ExampleController');
require('controllers/common/login/LoginController');
require('controllers/common/login/LogoutController');
require('controllers/common/menu/MenuController');
require('controllers/common/notfound/NotfoundController');
require('controllers/common/unauthorized/UnauthorizedController');

// App controllers
require('controllers/home/HomeController');

require('controllers/roles/RoleController');
require('controllers/roles/EditRoleController');

require('controllers/users/UserController');
require('controllers/users/EditUserController');

require('controllers/administrators/AdministratorController');
require('controllers/administrators/EditAdministratorController');