'use strict';

require('angular');
require('modules/Home');
require('services/HomeService');

function HomeController(model) {
  var self = this;

  this.title = 'Showmanager';
}

module.exports = angular
  .module('app.home')
  .controller('HomeController', [
    'homeService',
    HomeController
  ]);
