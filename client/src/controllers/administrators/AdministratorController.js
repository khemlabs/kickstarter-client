'use strict';

require('angular');
require('directives/ngEnter');
require('modules/Administrators');
require('services/AdministratorsService.js');

function AdministratorController( $mdDialog, model, adminModel ) {
  var self = this;

  this.title = 'Administradores';
  this.elements = [];
  this.selected = [];
  this.searchData = {};

  this.auth = model.getAuthObject();

  this.edit = function(id) {
    adminModel.edit(id);
  };

  this.onOrderChange = function (column) {
    self.deferred = model.onOrderChange(column).then(function(response){
      self.elements = response.data;
    });
  };

  this.searchByText = function() {
    this.deferred = model.searchByText(this.searchData.text).then(function(response){
      self.elements = response.data.filter(function(user){
        return user.type !== 'admin' && user.type !== 'superadmin';
      });
    });
  };

  this.search = function(query) {
    self.deferred = model.search(query).then(function(response){
      self.elements = response.data.filter(function(user){
        return user.type == 'admin' || user.type == 'superadmin';
      });
    });
  };

  this.delete = function(ev) {
    var title = self.selected.length > 1
        ? '¿Esta seguro que quiere eliminar los siguientes administradores?'
        : '¿Esta seguro que quiere eliminar al siguiente administrador?';
    var label = 'Eliminación de administradores';
    var content = '';
    for(var idx in self.selected){
      content += '<br>' + self.selected[idx].username;
    }
    model.openDialogDelete(ev, $mdDialog, title, content, label,
      function(){
        self.deferred = model.bulkDelete(self.selected).then(function(elements) {
          var data = model.removeElementsFromArray(self.elements, self.selected, elements);
          self.elements = data.elements;
          self.selected = data.selected;
        }, function(err){
          console.log(err);
          model.getErrorResponse(err);
        });
    });
  };

  this.search();
}

module.exports = angular
  .module('app.administrators')
  .controller('AdministratorController', [
    '$mdDialog',
    'userService',
    'administratorsService',
    AdministratorController
  ]);

// The inclusion of circular dependencies must be performed after "module.exports"
require('services/UserService.js');
