'use strict';

//var interceptor = function ( jwtInterceptorProvider, jwtOptionsProvider, $httpProvider ) {
var interceptor = function ( jwtInterceptorProvider, $httpProvider ) {
  
  jwtInterceptorProvider.tokenGetter = function(store) {
    return store.get('jwt');
  }
  // Fix "As of v0.1.0, requests to domains other than the application's origin must be white listed"
  //jwtOptionsProvider.config({whiteListedDomains: 'http://localhost:3001'});

  $httpProvider.interceptors.push('jwtInterceptor');
  
  // todo: response by interceptor
  //$httpProvider.interceptors.push(function() {
  //  return {
  //    request: function(config) {
  //      console.log(config);
  //      return config;
  //    },
  //    response: function(response) {
  //      return response;
  //    }
  //  };
  //});
  
};

var mdconfig = function ( $mdThemingProvider, $mdIconProvider ){
  $mdIconProvider
    .defaultIconSet(        "./assets/images/svg/avatars.svg"     , 128)
    .icon(  "menu"        , "./assets/images/svg/menu.svg"        , 24  )
    .icon(  "share"       , "./assets/images/svg/share.svg"       , 24  )
    .icon(  "google_plus" , "./assets/images/svg/google_plus.svg" , 512 )
    .icon(  "hangouts"    , "./assets/images/svg/hangouts.svg"    , 512 )
    .icon(  "twitter"     , "./assets/images/svg/twitter.svg"     , 512 )
    .icon(  "phone"       , "./assets/images/svg/phone.svg"       , 512 );

  $mdThemingProvider.theme('default')
    .primaryPalette('light-green', {default: "800"})
    .accentPalette('cyan');
};

var defaultRoutes = function ( $stateProvider, $urlRouterProvider ){
  // Defaults roots
  $stateProvider
    .state("home", {
      url         : "/home",
      templateUrl : "/templates/home/home.html",
      controller  : "HomeController",
      controllerAs: "home",
      authenticate: true,
      validTypes  : false
    })
    .state("root", {
      url         : "/",
      templateUrl : "/templates/common/login/login.html",
      controller  : "LoginController",
      controllerAs: "login",
      authenticate: false,
      validTypes  : []
    })
    .state("notfound", {
      url         : "/notfound",
      templateUrl : "/templates/common/notfound/notfound.html",
      controller  : "NotfoundController",
      controllerAs: "notfound",
      authenticate: false,
      validTypes  : []
    })
    .state("unauthorized", {
      url         : "/unauthorized",
      templateUrl : "/templates/common/unauthorized/unauthorized.html",
      controller  : "UnauthorizedController",
      controllerAs: "unauthorized",
      authenticate: false,
      validTypes  : []
    })
    .state("login", {
      url         : "/login",
      templateUrl : "/templates/common/login/login.html",
      controller  : "LoginController",
      controllerAs: "login",
      authenticate: false,
      validTypes  : []
    })
    .state("logout", {
      url         : "/logout",
      controller  : "LogoutController",
      controllerAs: "logout",
      authenticate: false,
      validTypes  : []
    });

  var routes = require('routes') || [];
  routes.forEach(function(route){
    $stateProvider.state(route.state, route.config);
  });

  $urlRouterProvider.otherwise("/notfound");
};

// Require app and routes
const app = require('app');

// Configure JWT interceptor
app.config([ 'jwtInterceptorProvider' , '$httpProvider', interceptor ]);

// Configure theme
app.config([ '$mdThemingProvider', '$mdIconProvider', mdconfig ]);

// Configure defaults routes
app.config([ '$stateProvider', '$urlRouterProvider', defaultRoutes ]);

// Load Common and controllers
require('modules/common/Common');
require('controllers/LoadControllers');
