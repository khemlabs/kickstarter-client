'use strict';

require('angular');

module.exports = angular.module('app.roles', [ 'ngMaterial', require('angular-material-data-table'), 'app.common.abm' ]);

// The inclusion of circular dependencies must be performed after "module.exports"
require('app'); // Make sure all common dependencies are loaded
