'use strict';

require('angular');

module.exports = angular.module('app.common.example', [ 'ngMaterial' ]);

// The inclusion of circular dependencies must be performed after "module.exports"
require('app'); // Make sure all common dependencies are loaded
