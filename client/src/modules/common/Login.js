'use strict';

require('angular');

module.exports = angular.module('app.common.login', [ 'angular-storage', 'app.common.auth' ]);

// The inclusion of circular dependencies must be performed after "module.exports"
require('app'); // Make sure all common dependencies are loaded
