var run = function (
  $rootScope,
  $mdComponentRegistry,
  $state,
  store,
  authService
) {
  // Bugfix show menu only when logged
  $rootScope.isLogged = function(states){
    return store.get('jwt') ? true : false;
  };
  $rootScope.isAuthorized = function(auth){
    return authService.isAuthorized(auth);
    return true;
  };
  // Bugfix div map shown
  $rootScope.isState = function(states){
    return states.indexOf($state.current.name) >= 0;
  }
  // Bugfix scrollable on menu open
  $rootScope.sideNavIsOpen = function(){
    return false;
  }
  $mdComponentRegistry.when('panel-menu').then(
    function(sideNav) {
      $rootScope.sideNavIsOpen = function() {
        return sideNav.isOpen();
      }
    }
  );
  authService.$stateChangeStart($rootScope, $mdComponentRegistry, $state, store);
};

module.exports = run;
