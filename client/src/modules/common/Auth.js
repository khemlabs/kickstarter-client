'use strict';

require('angular');

module.exports = angular.module('app.common.auth', [ 'angular-jwt', 'angular-storage' ]);

// The inclusion of circular dependencies must be performed after "module.exports"
require('app'); // Make sure all common dependencies are loaded
