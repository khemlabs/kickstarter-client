'use strict';

require('angular');
require('angular-material-data-table')

module.exports = angular.module('app.users', [ 'ngMaterial', 'md.data.table', 'app.common.abm' ]);

// The inclusion of circular dependencies must be performed after "module.exports"
require('app'); // Make sure all common dependencies are loaded
