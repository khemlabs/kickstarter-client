// Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

/**
* Facebook API wrapper
*
* @param conf {
*   [appID: string],
*   [cookie: boolean],
*   [xfbml: boolean],
*   [version: string],
*   [scope: string]
* } FB Configuration
*/
var facebookAPI = function(conf)
{
  var self = this;
  
  this.callback = {};
  this.conf     = conf;
  
  this.login = function() {    
    return new Promise(function (success, reject){
      self.checkLoginState().then(function(authResponse){
        return success(authResponse);
      }, function(err){
        if(err == 'not_logged'){
          self.__login(function(err, authResponse){
            if(err){
              return reject(err);
             } else {
               return success(authResponse);
             }
          });
         } else {
           return reject(err);
         }
      });
    });
  };
  
  this.checkLoginState = function() {
    return new Promise(function (success, reject){
      self.FB.getLoginStatus(function(response) {
        self.__statusChangeCallback(response, function(err, res){
          if (err) return reject(err);
          else return success(res);  
        });
      });
    });
  };
  
  this.__statusChangeCallback = function(response, callback) {
    if (response.status === 'connected') {
      return callback(null, response.authResponse);
    } else if (response.status === 'not_authorized') {
      return callback('not_authorized');
    } else {
      return callback('not_logged');
    }
  };
  
  this.__login = function(callback) {    
    self.FB.login(function(response) {
      if (response.authResponse) {
        return callback(null, response.authResponse);
      } else {
        return callback('canceled');
      }
    }, {
      scope: self.conf.scope || 'email,public_profile'
    });
  };
  
  this.onload = new Promise(function (success, reject){    
    window.fbAsyncInit = function() {
      self.FB = FB;

      self.FB.init({
        appId      : self.conf.appID,            // APP-ID
        cookie     : self.conf.cookie  || true,  // enable cookies to allow the server to access the session
        xfbml      : self.conf.xfbml   || true,  // parse social plugins on this page
        version    : self.conf.version || 'v2.5' // use graph api version 2.5
      });
      
      return success();
    };
  });
};