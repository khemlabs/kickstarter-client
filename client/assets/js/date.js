var weekday = new Array(7);

weekday[0] = "Domingo";
weekday[1] = "Lunes";
weekday[2] = "Martes";
weekday[3] = "Miércoles";
weekday[4] = "Jueves";
weekday[5] = "Viernes";
weekday[6] = "Sábado";

var months = new Array(12);

months[0]  = "Enero";
months[1]  = "Febrero";
months[2]  = "Marzo";
months[3]  = "Abril";
months[4]  = "Mayo";
months[5]  = "Junio";
months[6]  = "Julio";
months[7]  = "Agosto";
months[8]  = "Septiembre";
months[9]  = "Octubre";
months[10] = "Noviembre";
months[11] = "Diciembre";

Date.prototype.parseDateWithoutYear = function() {
  var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
  var dd  = this.getDate().toString();
  return (dd[1]?dd:"0"+dd[0]) + '/' + (mm[1]?mm:"0"+mm[0]); // padding
};

Date.prototype.parseDate = function() {
  var yyyy = this.getFullYear().toString();
  var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
  var dd  = this.getDate().toString();
  return yyyy + '-' + (mm[1]?mm:"0"+mm[0]) + '-' + (dd[1]?dd:"0"+dd[0]); // padding
};

Date.prototype.fullDate = function(arrayResponse){
  var dd = this.getDate();
  var mm = this.getMonth()+1; //January is 0!
  var yyyy = this.getFullYear();
  if(dd<10) dd='0'+dd;
  if(mm<10) mm='0'+mm;
  return arrayResponse ? [yyyy, mm, dd] : yyyy + '-' + mm + '-' + dd;
};

Date.prototype.subtract = function(days){
  this.setDate(this.getDate() - days);
  return this;
};

Date.prototype.add = function(days){
  this.setDate(this.getDate() + days);
  return this;
};

Date.prototype.compare = function(date){
  return (this.fullDate() == date.fullDate());
};

Date.prototype.range = function(initial_date, final_date){
  var initial_date = initial_date.fullDate(true);
  var initial_date = new Date(initial_date[0], initial_date[1] - 1, initial_date[2]);
  var final_date = final_date.fullDate(true);
  var final_date = new Date(final_date[0], final_date[1] - 1, final_date[2]);
  return (final_date >= this && initial_date <= this); 
};

Date.prototype.dayDescription = function(){
  return weekday[this.getDay()];
};

function parseDateResponse(element) {
  var response = {};
  for(var key in element){
    try{
      var dateFromString = DateFromString(element[key]);
      response[key] = dateFromString;
      console.log('The key (%s) is a date, creating object from string', key);
    } catch(err){
      response[key] = element[key];
      console.log('The key (%s) IS NOT a date', key);
    }
  }
  return response;
}

function DateWithoutTime(dateString){
  var tpos = dateString.indexOf('T');
  if(tpos >= 0){
    return dateString.substring(0, tpos);
  }
  return dateString;
}

function DateFromString(dateString){
  var date = dateString.split("-");
  var pos = date[2].indexOf('T') >= 0 ? date[2].indexOf('T') : date[2].length;
  var day = date[2].substring(0, pos);
  return new Date(date[0], date[1] - 1, day);
}

function today(){
 return new Date();
}

function todayNumber(){
 return today().getDate();
}

function getMonth(month){
 return months[month]; 
}

function addMinutes(date, minutes) {
  return new Date(date.getTime() + minutes*60000);
}