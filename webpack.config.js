var config = {
  resolve: {
    root: [__dirname + "/client/src/"]
  },
  context: __dirname + "/client/src/",
  entry: "./entrypoint.js",
  output: {
    path: __dirname + "/dist",
    filename: "bundle.js"
  },
  module: {
    loaders: [
      { test: /\.css$/, loader: "style-loader!css-loader" },
      { test: /\.scss$/, loaders: ["style", "css", "sass"]}
    ]
  }
};

module.exports = config;
