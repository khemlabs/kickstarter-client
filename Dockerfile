############################################################
# Dockerfile to build NodeJS 4 Installed Containers
# Based on Node:4.4.3
############################################################

FROM khemlabs/kickstarter-client-base

ARG API_HOST=/
ARG LISTEN_PORT=3000

# Copy application folder and configurations
COPY . /app

# Create data directory
WORKDIR /app

# npm install
RUN npm install

# Run gulp
RUN gulp init --API_HOST=$API_HOST

CMD ["forever", "server.js"]
