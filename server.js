// Create and configure server

var express           = require('express')
    ,app              = express()
    ,http             = require('http');

var server = http.createServer(app);

app.use(express.static('dist'));

server.listen(process.env.PORT || 3000, function() {
  var address = server.address().address;
  var host = address && address !== '::' ? address : 'localhost',
      port = server.address().port;
  console.info('[app] Listening at http://%s:%s', host, port);
});

module.exports = app;
