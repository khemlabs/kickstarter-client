// Create and configure server

var models  = require(__dirname + '/model-importer').getModels()
    ,cp     = require('child_process')
    ,fs     = require('fs')
    ,async  = require('async');

/*
* GENERATE FRONTEND FILES
* -----------------------
*
* Warning:
* --------
* If you are not sure how to use this generator we recomend you to exec the command line tool
* gulp generate-frontend --modelname=MODEL_NAME [--keyname=KEY_COLUMN_NAME] [--idname=ID_COLUMN_NAME]
*
* MODEL_NAME must be in camelcase
* If keyname is not recived "description" will be used
* If idname is not recived "lower(MODEL_NAME)_id" will be used
*/

/**
* filesGenerator imports all files with format [model].json
* witch are in the directory lib/models/ and generate the files
* needed in the frontend.
*/
var filesGenerator = function() {
  var self      = this;

  this.executeCommand = function( generator, data, callback ) {
    var command = 'gulp generate-frontend' + ' --modelname=' + generator.name;
    if(generator.key)     command += " --key='"     + generator.key     + "'";
    if(generator.id)      command += " --id='"      + generator.id      + "'";
    if(generator.title)   command += " --title='"   + generator.title   + "'";
    if(generator.keyname) command += " --keyname='" + generator.keyname + "'";
    if(generator.columns) command += " --columns='" + JSON.stringify(generator.columns) + "'";
    if(generator.auth)    command += " --auth='true'";
    if(generator.import)  {
      command += " --import='true'";
      console.log('Imports are required, install npm dependencies (ng-csv@0.3.6 ng-file-upload@12.2.5)...');
      cp.exec('npm install ng-csv@0.3.6 ng-file-upload@12.2.5 --save', function(error, stdout, stderr) {
        if(error) console.log('npm install failed');
        else {
          data.imports = true;
          
        }
        return self.compile(generator, models, command, callback);
      }); 
    } else {
      return self.compile(generator, models, command, callback);
    }
  };

  this.compile = function(generator, models, command, callback){
    if(generator.associations && generator.associations.length){
      var associations = [];
      for(var i = 0; i < generator.associations.length; i++){
        if(generator.associations[i].type 
           && generator.associations[i].modelname 
           && models[generator.associations[i].modelname]
           && models[generator.associations[i].modelname].name
        ){
          var data = models[generator.associations[i].modelname];
          var association = {
            required  : generator.associations[i].required  || false,
            title     : generator.associations[i].title     || 'Asociar',
            showIf    : generator.associations[i].showIf    || [],
            key       : data.key                            || false,
            id        : data.id                             || false,
            keyname   : data.keyname                        || false,
            auth      : data.auth                           || false,
            import    : data.import                         || false,
            modelname : data.name,
            type      : generator.associations[i].type
            
          }
          associations.push(association);
        } else {
          console.log('Association not valid (modelname and type params are required)');
        }
      }
      command += " --associations='" + JSON.stringify(associations) + "'";
    }
    console.log('Exec: ' + command);
    cp.exec(command, function(error, stdout, stderr) {
      if(error) console.log('File generation failed (check if files already exist): ' + generator.name);
      else if(stdout) {
        console.log('Files generated: (' + generator.name + ')');
      }
      return callback();
    });  
  };
  
  this.generateFiles = function() {
    var commandline = '';
    var data = {
      imports: false  
    };
    async.eachSeries(models, function(model, callback) {
      if(model.compile !== false){
        if(model.delete){
          console.log('Deleting previous files: ', model.name);
          var command = "gulp delete-frontend --modelname='" + model.name + "'";
          cp.exec(command, function(error, stdout, stderr) {
            if(error) return callback( error );
            else {
              return self.executeCommand( model, data, callback );
            }
          });
        } else {
          return self.executeCommand( model, data, callback );
        }
      } else {
        console.log('Model ' + model.name + ' with compile variable in false');
        return callback();
      }
    }, function done(err) {
      if(err) console.error(err);
      else {
        console.log(' ');
        console.log('***** File generation success *****');
        console.log(' ');
        console.log('***************************** IMPORTANT *********************************');
        console.log('* Please read "Update client files" in "README.md" before compiling app *');
        if(data.imports){
          console.log('****************************** IMPORTS **********************************');
          console.log('* Please read "Routes with imports" in "README.md" before compiling app *');
        }
        console.log('*************************************************************************');  
      }
    });
  }

  this.generateFiles();
};

module.exports = new filesGenerator();
