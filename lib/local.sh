#!/bin/bash

echo '********** INIT lib/local.sh **********'

NPM_PATH=$(pwd)/node_modules/
echo 'Searching for path '$NPM_PATH

echo '********** Check if node_modules exist **********'
if [ -d "$NPM_PATH" ]; then
  echo 'node_modules exist'
else
  echo 'node_modules does not exist, execute "npm install"'
  npm install
fi

echo '********** RUN SERVER **********'
forever server.js &

echo '********** Init GULP ********** '
echo 'Init client app'
gulp init-dev --API_HOST=$API_HOST --DEV=true

