(function(){
  'use strict';

  // Prepare the 'users' module for subsequent registration of controllers and delegates
  angular.module('<%= moduleName %>', [ require('angular-material-data-table'), 'ngMaterial', 'app.common.abm' ]);

})();
