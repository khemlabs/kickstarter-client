'use strict';

require('angular');
require('directives/ngEnter');
require('modules/<%= singleNameCap %>');
require('services/<%= singleNameCap %>Service');

function <%= controllerName %>( $q, $mdDialog, model ) {
  var self = this;

  this.title = '<%= title %>';
  this.elements = [];
  this.selected = [];
  this.searchData = {};

  <% if (auth) { %>this.auth = model.getAuthObject();<% } %>

  this.edit = function(id) {
    model.edit(id);
  };

  this.onOrderChange = function (column) {
    self.deferred = model.onOrderChange(column).then(function(response){
      self.elements = response.data.map(function(element){
        <% _.each(columns, function(column){ %><% if (column.type == 'date') { %>element.<%=column.key%> = new DateFromString(element.<%=column.key%>);<%}%>
        <% }); %>
        return element;
      });
    });
  };

  this.searchByText = function() {
    this.deferred = model.searchByText(this.searchData.text).then(function(response){
      self.elements = response.data.map(function(element){
        <% _.each(columns, function(column){ %><% if (column.type == 'date') { %>element.<%=column.key%> = new DateFromString(element.<%=column.key%>);<%}%>
        <% }); %>
        return element;
      });
    });
  };

  /**
  * This function allows extra param disabled.
  *
  * @associationID
  * @exclude array of objects to exlude of search
  * @disabled boolean that enables "disabled results"
  *
  * @warning: exclude and disabled need middleware implementation on server to work.
  *
  * @example: server/middlewares/Example.middleware.js in list.start
  */
  this.searchForAutocomplete = function(associationID, exclude) {
    return $q(function(resolve, reject){
      if(self.lastSearch != self.searchData.text){
        self.lastSearch = self.searchData.text;
        // If you want to recive disabled param, pass it to this function
        return model.searchForAutocomplete(self.searchData.text, associationID, exclude).then(function(data){
          self.searchCache = data;
          return resolve(self.searchCache);
        });  
      } else {
        return resolve(self.searchCache);
      }
    });
  };

  this.clearAutocomplete = function() {
    self.lastSearch   = null;
    self.searchCache  = [];
  };

  this.search = function(query) {
    self.deferred = model.search(query).then(function(response){
      self.elements = response.data.map(function(element){
        <% _.each(columns, function(column){ %><% if (column.type == 'date') { %>element.<%=column.key%> = new DateFromString(element.<%=column.key%>);<%}%>
        <% }); %>
        return element;
      });
    });
  };

  this.delete = function(ev) {
    var title = self.selected.length > 1
        ? '¿Esta seguro que quiere eliminar los siguientes <%= pluralName %>?'
        : '¿Esta seguro que quiere eliminar al siguiente <%= singleName %>?';
    var label = 'Eliminación de <%= pluralName %>';
    var content = '';
    for(var idx in self.selected){
      content += '<br>' + self.selected[idx].<%= key %>;
    }
    model.openDialogDelete(ev, $mdDialog, title, content, label,     
      function(){
        self.deferred = model.bulkDelete(self.selected).then(function(elements) {
          var data = model.removeElementsFromArray(self.elements, self.selected, elements);
          self.elements = data.elements;
          self.selected = data.selected;  
        }, function(err){
          console.log(err);
          model.getErrorResponse(err);
        });
    });
  };

  <% if (importCode) { %>this.import = function(ev) {
    model.openDialogEdit(ev, $mdDialog, '<%= importControllerName %>', '<%= importControllerAs %>', '<%= importViewPath %>', {},
      function(element){
        if(element) self.search();
      });
  };<% } %>

  this.search();
}
        
module.exports = angular
  .module('<%= moduleName %>')
  .controller('<%= controllerName %>', [
    '$q',
    '$mdDialog',          
    '<%= serviceName %>',
    <%= controllerName %>
  ]);