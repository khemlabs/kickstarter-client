'use strict';

require('angular');
require('factories/focus');
require('modules/<%= singleNameCap %>');
require('services/<%= singleNameCap %>Service');

function <%= editControllerName %>( focus, $params, model ) {
  var self = this;

  focus('<%= editControllerName %>');

  this.callback = $params.callback || '<%= pluralName %>';

  this.loading = true;
  <%if(associations && associations.length){%><% _.each(associations, function(association){ %>
  this.<%=association.pluralName%>Selected = [];<% }); %>
  <% } %>
  this.resetParams = function(element){
    if(element) {
      // Parse element columns, if no columns to modify, please remove if statement
      <% _.each(columns, function(column){ %><% if (column.type == 'date') { %>element.<%=column.key%> = new DateFromString(element.<%=column.key%>);<%}%><% }); %>
    }
    self.data               = element || {};
    self.newInput           = element ? false : true;
    self.data.creation_date = self.data.creation_date || new Date();
    self.loading            = false;
    
    self.requiredAssociations = {
      <% _.each(associations, function(association){ %><%if(association.required){%><%if(association.type == 'list'){ %>'<%=association.pluralNameCap%>': {
        data:   self.data.<%=association.pluralNameCap%>,
        error: !self.data.<%=association.pluralNameCap%> || !self.data.<%=association.pluralNameCap%>.length,
        dirty: false,
        showIf: <%=JSON.stringify(association.showIf) || []%>}<%} else if(association.type == 'input') { %>'<%=association.singleNameCap%>': {
        data:   self.data.<%=association.singleNameCap%>,
        error: !self.data.<%=association.singleNameCap%>,
        dirty: false,
        showIf: <%=JSON.stringify(association.showIf) || []%>
      }<%}%>,<%}%><%});%>
    };
  };

  this.save = function(params) {
    if(self.addForm.$valid){
      model.save(params).then(function(response){
        model.parseSaveResponse(response, false, self);
        model.returnToList(self.callback);
      }, function(response){
        model.getErrorResponse(response, self.addForm);
      }); 
    }
  };

  this.loadElement = function(){
    if(!$params.id){
      self.resetParams();
    } else {
     model.get($params.id).then(function(element){
        self.resetParams(element);
      }, function(err){
        model.returnToList(self.callback);
      });
    }
  };
<%if(associations && associations.length){%>
  self.checkRequiredAssociations = function() {
    self.addForm.$submit = true;
    if(self.requiredAssociations){
      for(var key in self.requiredAssociations){
        var show = true;
        if(self.requiredAssociations[key].showIf.length){
          show = false;
          for(var i = 0; (!show && i < self.requiredAssociations[key].showIf.length); i++){
            var column  = self.requiredAssociations[key].showIf[i].key;
            var value   = self.requiredAssociations[key].showIf[i].value || false;
            if( (value && self.data[column] && self.data[column] == value) || (!value && self.data[column] && self.data[column]) )
              show = true;
          }  
        }
        if( show && self.requiredAssociations[key].error ){
          self.requiredAssociations[key].dirty = true;
          return false;
        }
      }
    }
    return true;
  };

  this.setAssociation = function(search, association, associationName, associationID) {
    if(association){
      self.data[associationName]  = association;
      self.addForm.$dirty         = true;
      search.text                 = '';
      var name                    = 'showAdd' + associationName;
      self[name]                  = false;
      if(self.requiredAssociations[associationName]){
        self.requiredAssociations[associationName].error = false;
        self.requiredAssociations[associationName].dirty = true;
      }
    }
  };

  this.addAssociation = function(search, association, clearFunction, associationName, associationID) {
    var added = model.addAssociation(self.data, association, associationName, associationID);
    if(added){
      self.addForm.$dirty = true;
      search.text         = '';
      association         = undefined;
      if(self.requiredAssociations[associationName]){
        self.requiredAssociations[associationName].error = false;
        self.requiredAssociations[associationName].dirty = true;
      }
      clearFunction();
    }
  };

  this.removeAssociations = function(data, associationKEY, associationName, associationID) {
    var removed = model.removeAssociations(self.data, data[associationKEY], associationName, associationID);
    if(removed){
      self.addForm.$dirty = true;
      data[associationKEY]  = [];
      if(self.requiredAssociations[associationName]){
        self.requiredAssociations[associationName].error = self.requiredAssociations[associationName].length ? false : true
        self.requiredAssociations[associationName].dirty = true;
      }
    }
  };<% } %>

  this.loadElement();

}

module.exports = angular
  .module('<%= moduleName %>')
  .controller('<%= editControllerName %>', [
    'focus',
    '$stateParams',
    '<%= serviceName %>',
    <%= editControllerName %>
  ]);
