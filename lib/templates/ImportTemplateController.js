'use strict';

require('angular');
require('factories/focus');
require('modules/<%= singleNameCap %>');
require('services/<%= singleNameCap %>Service');
require('services/common/FilesService');

function <%= importControllerName %>(focus, $mdDialog, model, files, data) {    
  var self = this;
  this.csvColumns = [
  <% _.each(columns, function(column){ %>
    {description: '<%=column.key%>'<% if (column.required) { %>, required: true <% } %><% if (column.default)  { %>, default: '<%=column.default%>' <% } %>},
  <% }); %>
    {description: '<%=key%>', required: true, default: '<%=keyName%>'}
  ];

  this.uploading = false;

  focus('<%= importControllerName %>');
  this.data = data || {};

  this.getCsvValues = function() {
    var data = {};
    for(var i = 0; i < self.csvColumns.length; i++){
      if(self.csvColumns[i].required){
        data[self.csvColumns[i].description] = self.csvColumns[i].default ? self.csvColumns[i].default : 'Campo obligatorio';
      } else {
        data[self.csvColumns[i].description] = '';
      }
    }
    return [data];
  };

  this.getCsvHeader = function() {
    var data = [];
    for(var i = 0; i < self.csvColumns.length; i++){
      data.push(self.csvColumns[i].description);
    }
    return data;
  };

  // Start uploading the file
  this.uploadFile = function() {
    self.clearError();
    self.uploading = true;
    if (self.uploadForm.file.$valid && self.file) {
      files.import('<%= pluralName %>', self.file, self.uploadSuccess, self.uploadError);
    }
  };

  // The request ended
  this.uploadSuccess = function (data) {
    self.file = null;
    self.uploading = false;
    if(data.success){
      $mdDialog.hide(true);
    } else {
      self.errorUpload = true; 
    }
  };

  // The request failed
  this.uploadError = function (error) {
    switch(error.status){
      case 400: 
        self.fileNotUploaded = true;
      break;
      case 422: 
        self.errorFileType = true;
      break;
      case 500: 
        var parsed = model.parseErrorImport(error);
        switch(parsed.type){
          case 'unique':
            self.errorUnique = parsed.value;
            break;
          case 'not_found':
            self.errorColumns = parsed.data;
            break;
          case 'empty':
            self.errorEmpty = parsed.data;
            break;
          case 'validation':
            self.errorValidations = parsed.data;
            break;
          case 'entity':
            self.errorEntity = parsed.data;
            break;
          default:
            self.errorUpload = true;
            break;
        }
      break;
      default: 
        self.errorUpload = true;  
      break;
    }
    self.uploading  = false;
    self.file       = null;
  };

  this.clearError = function (){
    self.errorUnique      = false;
    self.fileNotUploaded  = false;
    self.errorFileType    = false;
    self.errorColumns     = false;
    self.errorUpload      = false;
    self.errorEmpty       = false;
    self.errorValidations = false;
    self.errorEntity      = false;
  };

  this.hide = function() {
    $mdDialog.hide(false);
  };
                                                                                                                
  this.cancel = function() {
    $mdDialog.cancel();
  };
}

module.exports = angular
  .module('<%= moduleName %>')
  .controller('<%= importControllerName %>', [
    'focus',
    '$mdDialog',
    '<%= serviceName %>',
    'filesService',
    'data',
    <%= importControllerName %>
  ]);
