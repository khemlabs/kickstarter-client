var fs = require('fs');

var modelImporter = function() {  
  var self = this;
  
  this.models      = {};
  this.modelsDIR   = __dirname + '/models/'
  this.filenames   = [];
  
  this.validFileName = function(filename) {
    var name = filename.split('.');
    if(name.length == 2 && name[0] != 'example' && name[1] == 'js') return name[0]
    else if (name[0] != 'example'){
      console.error('The file ' + filename + ' has an invalid name');
    }
    return false;
  };
  
  this.checkFiles = function() {
    self.filenames = fs.readdirSync( self.modelsDIR );
    for(var i = 0; i < self.filenames.length; i++){
      var filename = self.filenames[i];
      var name = self.validFileName(filename)
      if(name){
        console.log('Fetching model json file: ' + filename);
        try{
          self.models[name] = require(self.modelsDIR + filename);
        } catch (err){
          console.error(err);
        }
      }
    }
    return self.models;
  }
  
  this.getModels = function(){
    console.log('Importing models...');
    return self.checkFiles();
  }
  
};

module.exports = new modelImporter();