/**
* JSON example needed for files generation
* @param delete [optional] {boolean} Deletes the existing paths before creating new ones
* @param compile [optional] {boolean} If false, files will not be generated
* @oaram title [optional] {string} Title to show on site
* @param name {string} Is the name of this model in camelcase and singular
* @param key [optional, default: 'description'] {string} Is the most important column of the model, needed for searchs.
* @param keyname [optional, default: 'description'] {string} Is the text to show on the view
* @param [optional, default: '{name}_id'] id {string} Is the name of the primary key
* @param columns [optional] {array} Is a list of columns to show in the edit and list view
* @param associations [optional] 
* @Example: [ 
*   {
*     name:  'MyModel'
*     type:  'list',
*     [required: [true, false]],
*     [title:  'Add MyModel association'],
*     [showIf: 
*       [
*         {
*           key: 'column',
*           [notEmpty: [true,false]],
*           [value: 'my_value']
*         }
*       ]
*     ]
*   },
*   {
*     model:  orm.models.MyOtherModel, 
*     type:   'input',
*     [title:  'Set MyOtherModel association'],
*     [
*       showIf: 
*         [
*           {
*             key: 'column',
*             [notEmpty: [true,false]],
*             [value: 'my_value']
*           }
*         ]
*     ]
*   }
* ]
*/
module.exports = {
  name    : 'Example',     // Must be in camelcase
  title   : 'Example app', // Title of the view
  auth    : false,         // User must be authorized
  import  : true,          // Create import button (import method must be develop)
  delete  : false,         // Delete previous files
  compile : false,         // Generate files
  id      : 'example_id',  // optional (default: [file_name]_id}
  key     : 'description', // optional (default: [description])
  keyname : 'example',     // optional (default: [description])
  columns : [              // optional
    {
      key: 'example', 
      name: 'example', 
      required: true,
      unique: false, 
      type: 'string'
    }
  ]
};