var gulp        = require('gulp');
var runSequence = require('run-sequence');
var argv        = require('yargs').argv;
var fs          = require('fs');
var pluralize   = require('pluralize');

// ----- HELPERS -----

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function getPaths(){
  var nameC  = argv.modelname;
  var name   = nameC.toLowerCase();
  var plural = pluralize(name);
  return { 
    controllers : 'client/src/controllers/' + plural + '/',
    modules     : 'client/src/modules/',
    services    : 'client/src/services/',
    templates       : 'client/src/templates/' + plural + '/'  
  }
}

function getDeleteParams(){
  var nameCap   = argv.modelname;
  var name      = nameCap.toLowerCase();
  var pluralCap = pluralize(nameCap);
  var plural    = pluralCap.toLowerCase();
  return {
    name      : name,
    nameCap   : nameCap,
    plural    : plural,
    pluralCap : pluralCap
  };
}

function deleteControllers() {
  var paths   = getPaths();
  var params  = getDeleteParams();
  try {
    fs.accessSync( paths.controllers, fs.F_OK );
    fs.unlinkSync ( paths.controllers             + params.nameCap + 'Controller.js' );
    fs.unlinkSync ( paths.controllers + '/Edit'   + params.nameCap + 'Controller.js' );
    fs.unlinkSync ( paths.controllers + '/Import' + params.nameCap + 'Controller.js' );
    fs.rmdirSync  ( paths.controllers );
    return false;
  } catch (err) {
    return err;
  };
}

function deleteModule() {
  var paths   = getPaths();
  var params  = getDeleteParams();
  try {
    fs.accessSync( paths.modules  + params.nameCap + '.js', fs.F_OK );
    fs.unlinkSync ( paths.modules + params.nameCap + '.js' );
    return false;
  } catch (err) {
    return err;
  };
}

function deleteService() {
  var paths   = getPaths();
  var params  = getDeleteParams();
  try {
    fs.accessSync( paths.services + params.nameCap + 'Service.js', fs.F_OK );
    fs.unlinkSync ( paths.services + params.nameCap + 'Service.js' );
    fs.rmdirSync  ( paths.services );
    return false
  } catch (err) {
    return err;
  };
}

function deletetemplates() {
  var paths   = getPaths();
  var params  = getDeleteParams();
  try {
    fs.accessSync( paths.templates + 'edit'        + '.html', fs.F_OK );
    fs.unlinkSync( paths.templates + 'edit'        + '.html' );
  } catch (err) {
    console.log(err);
  };
  try {
    fs.accessSync( paths.templates + 'import'        + '.html', fs.F_OK );
    fs.unlinkSync( paths.templates + 'import'      + '.html' );
  } catch (err) {
    console.log(err);
  };
  try {
    fs.accessSync( paths.templates + params.plural + '.html', fs.F_OK );
    fs.unlinkSync( paths.templates + params.plural + '.html' );
  } catch (err) {
    console.log(err);
  };
  return false;
}

// ----- CHECK PARAMS AND PATH'S FOR DELETE -----

gulp.task('check-delete-params', function(callback) {
  if(!argv.modelname){
    return callback('Param (--modelname) required: not found');
  }  
  return callback();
});

gulp.task('delete-paths', function(callback) {
  var err = deleteControllers();
  if(err) console.log(err);
  
  err = deleteModule();
  if(err) console.log(err);
  
  err = deleteService();
  if(err) console.log(err);      
  
  err = deletetemplates();
  if(err) console.log(err);
  
  return callback();
});

// ----- DELETE FRONTEND FILES -----

gulp.task('delete-frontend', function(callback) {
  runSequence(
    'check-delete-params',
    'delete-paths',
    callback
  );
});