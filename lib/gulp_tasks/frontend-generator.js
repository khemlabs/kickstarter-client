var gulp        = require('gulp');
var runSequence = require('run-sequence');
var template    = require('gulp-template');
var rename      = require('gulp-rename');
var mkdirp      = require('mkdirp');
var argv        = require('yargs').argv;
var pluralize   = require('pluralize');
var fs          = require('fs');

// ----- STATIC PARAMS -----

var requiredParams = ['modelname'];

var defaultColumns = [{key: 'test', name: 'Prueba', required: true, unique: false, type: 'text'}];

var templatePaths = {
  controllers: {
    list:   'lib/templates/ListTemplateController.js',
    edit:   'lib/templates/EditTemplateController.js',
    import: 'lib/templates/ImportTemplateController.js'
  },
  templates: {
    list:   'lib/templates/listTemplate.html',
    edit:   'lib/templates/editTemplate.html',
    import: 'lib/templates/importTemplate.html'
  },
  modules:   'lib/templates/TemplateModule.js',
  services:  'lib/templates/TemplateService.js'
};

// ----- HELPERS -----

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function getPaths(){
  var nameC   = argv.modelname;
  var name    = nameC.toLowerCase();
  var pluralC = pluralize(nameC);
  var plural  = pluralC.toLowerCase();
  return {
    controllers : 'client/src/controllers/' + plural + '/',
    modules     : 'client/src/modules/',
    services    : 'client/src/services/',
    templates       : 'client/src/templates/' + plural + '/'
  }
}

function parseModelData(data){
  var nameC   = data.modelname;
  var name    = nameC.toLowerCase();
  var pluralC = pluralize(nameC);
  var plural  = pluralC.toLowerCase();

  var id      = data.id  ? data.id.toLowerCase()  : name + '_id';
  var key     = data.key ? data.key.toLowerCase() : 'description';
  var keyname = data.keyname  || capitalizeFirstLetter( key );
  var title   = data.title    || pluralC;

  return {
    auth                 : data.auth   ? true : false,
    importCode           : data.import ? true : false,
    singleName           : name,
    pluralName           : plural,
    singleNameCap        : nameC,
    pluralNameCap        : pluralC,
    id                   : id,
    key                  : key,
    keyName              : keyname,
    title                : title,
    controllerName       : nameC              + 'Controller',
    serviceName          : name               + 'Service',
    moduleName           : 'app.'             + plural,
    editControllerName   : 'Edit'    + nameC  + 'Controller',
    editControllerAs     : 'edit'             + name,
    editViewPath         : 'templates/'  + plural + '/edit.html',
    editKey              : 'edit'    + name   + '.data.'  + key,
    editName             : 'edit'             + name,
    editId               : 'edit'    + name   + '.data.'  + id,
    importControllerName : 'Import'  + nameC  + 'Controller',
    importControllerAs   : 'import'           + name,
    importViewPath       : 'templates/'  + plural + '/import.html',
    importName           : 'import'           + name
  }
}

function getAssociations(associations){
  associations = JSON.parse(associations);
  var result = [];
  for(var i = 0; i < associations.length; i++){
    var data      = parseModelData(associations[i]);
    data.type     = associations[i].type;
    data.title    = associations[i].title     || data.pluralNameCap;
    data.showIf   = associations[i].showIf    || [];
    data.required = associations[i].required  || false;
    result.push( data );
  }
  return result;
}

function getParams(){
  if(argv.columns && typeof argv.columns == 'string'){
    argv.columns = JSON.parse( argv.columns );
  }
  argv.columns        = argv.columns || defaultColumns;
  var result          = parseModelData(argv);
  result.columns      = argv.columns;
  result.associations = (argv.associations)
    ? getAssociations(argv.associations)
    : [];
  return result;
}

function compileFile(src, params, renamePath, dest){
  return gulp.src( src )
    .pipe( template( params ) )
    .pipe( rename( renamePath ) )
    .pipe( gulp.dest( dest ) );
}

// ----- CHECK PARAMS AND PATH'S -----

gulp.task('log-params', function(callback) {
  console.log( getParams() );
});

gulp.task('check-params', function(callback) {
  for(var i = 0; i < requiredParams.length; i++){
    if(!argv[requiredParams[i]]){
      return callback('Param required not found: ' + requiredParams[i]);
    }
  }
  return callback();
});

gulp.task('check-paths', function(callback) {
  var paths = getPaths();
  var params = getParams();
  try {
    fs.accessSync(paths.controllers + params.controllerName + '.js', fs.F_OK);
    return callback(paths.controllers + params.controllerName + '.js already exist');
  } catch (e) {
    try {
      fs.accessSync(paths.controllers + params.editControllerName + '.js', fs.F_OK);
      return callback(paths.controllers + params.editControllerName + '.js already exist');
    } catch (e) {
      try {
        fs.accessSync(paths.controllers + params.importControllerName + '.js', fs.F_OK);
        return callback(paths.controllers + params.importControllerName + '.js already exist');
      } catch (e) {
        try {
          fs.accessSync(paths.templates + 'edit.html', fs.F_OK);
          return callback(paths.templates + 'edit.html' + ' already exist');
        } catch (e) {
          try {
            fs.accessSync(paths.templates + 'import.html', fs.F_OK);
            return callback(paths.templates + 'import.html' + ' already exist');
          } catch (e) {
            try {
              fs.accessSync(paths.templates + params.pluralName + '.html', fs.F_OK);
              return callback(paths.templates + params.pluralName + '.html already exist');
            } catch (e) {
              try {
                fs.accessSync(paths.modules + params.singleNameCap + '.js', fs.F_OK);
                return callback(paths.modules + params.singleNameCap + '.js');
              } catch (e) {
                try {
                  fs.accessSync(paths.services + params.singleNameCap + 'Service.js', fs.F_OK);
                  return callback(paths.services + params.singleNameCap + 'Service.js');
                } catch (e) {
                  return callback();
                }
              }
            }
          }
        }
      }
    }
  }
});

gulp.task('build-paths', function(callback) {
  var paths = getPaths();
  mkdirp(paths.controllers, function (err) {
    return callback(err);
  });
});

// ----- GENERATE CONTROLLERS -----

gulp.task('generate-list-controller', function(callback) {
  var params = getParams();
  return compileFile(
    templatePaths.controllers.list,
    params,
    params.singleNameCap + "Controller.js",
    getPaths().controllers
  );
});

gulp.task('generate-edit-controller', function(callback) {
  var params = getParams();
  return compileFile(
    templatePaths.controllers.edit,
    params,
    "Edit" + params.singleNameCap + "Controller.js",
    getPaths().controllers
  );
});

gulp.task('generate-import-controller', function(callback) {
  var params = getParams();
  if(params.importCode){
    return compileFile(
      templatePaths.controllers.import,
      params, "Import" + params.singleNameCap + "Controller.js",
      getPaths().controllers
    );
  } else {
    console.log('Import controller will not be created, import value is false');
    return callback();
  }
});

// ----- GENERATE MODULE AND SERVICE -----

gulp.task('generate-module', function(callback) {
  var params = getParams();
  return compileFile(
    templatePaths.modules,
    params,
    params.singleNameCap + ".js",
    getPaths().modules
  );
});

gulp.task('generate-service', function(callback) {
  var params = getParams();
  return compileFile(
    templatePaths.services,
    params,
    params.singleNameCap + "Service.js",
    getPaths().services
  );
});

// ----- GENERATE templates -----

gulp.task('generate-list-view', function(callback) {
  var params = getParams();
  return compileFile(
    templatePaths.templates.list,
    params,
    params.pluralName + ".html",
    getPaths().templates
  );
});

gulp.task('generate-edit-view', function(callback) {
  var params = getParams();
  return compileFile(
    templatePaths.templates.edit,
    params,
    "edit.html",
    getPaths().templates
  );
});

gulp.task('generate-import-view', function(callback) {
  var params = getParams();
  if(params.importCode){
    return compileFile(
      templatePaths.templates.import,
      params,
      "import.html",
      getPaths().templates
    );
  } else {
    console.log('Import view will not be created, import value is false');
    return callback();
  }
});

// ----- GENERATE FRONTEND FILES -----

gulp.task('generate-frontend', function(callback) {
  runSequence(
    'check-params',
    'check-paths',
    'build-paths',
    'generate-list-controller',
    'generate-edit-controller',
    'generate-import-controller',
    'generate-module',
    'generate-service',
    'generate-list-view',
    'generate-edit-view',
    'generate-import-view',
    callback
  );
});
