var gulp        = require('gulp');
var sourcemaps  = require('gulp-sourcemaps');
var template    = require('gulp-template');
var rename      = require('gulp-rename');
var webpack     = require('webpack-stream');
var htmlmin     = require('gulp-htmlmin');
var spawn       = require('child_process').spawn;
var argv        = require('yargs').argv;
var node;

// ********** NODEJS CONFIGURATION **********

var restart = function(){
  console.log('node app.js');
  node = spawn('node', ['app.js'], {stdio: 'inherit'})
  node.on('close', function (code) {
    if (code === 8) {
      console.log('Error detected, waiting for changes ...');
    }
  });
};

gulp.task('restart', function() {
  if (node) {
    console.log('********** Restarting nodeJS **********');
    node.kill();
    restart();
  } else {
    console.log('********** Starting nodeJS **********');
    restart();
  }
});

// ********** PROJECT CONFIGURATION **********

gulp.task('html', function() {
  return gulp.src('client/src/templates/**/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest('dist/templates'));
});

gulp.task('index', function() {
  return gulp.src('client/index.html')
    .pipe(gulp.dest('dist/'));
});

gulp.task('env-js', function() {
  var apiUrl = '/';
  if(argv.API_HOST){
    apiUrl = argv.API_HOST.trim();
    if(apiUrl.charAt(apiUrl.length - 1) != '/'){
      apiUrl += '/';
    }
  }
  console.log('--> API URL AT: %s', apiUrl);
  return gulp.src( 'lib/templates/env.js' )
    .pipe( template( {apiUrl: apiUrl} ) )
    .pipe( rename( 'env.js' ) )
    .pipe( gulp.dest( 'dist/' ) );
});

gulp.task('env-js', function() {
  var apiUrl = '/';
  if(argv.API_HOST){
    apiUrl = argv.API_HOST.trim();
    if(apiUrl.charAt(apiUrl.length - 1) != '/'){
      apiUrl += '/';
    }
  }
  console.log('--> API URL AT: %s', apiUrl);
  return gulp.src( 'lib/templates/env.js' )
    .pipe( template( {apiUrl: apiUrl} ) )
    .pipe( rename( 'env.js' ) )
    .pipe( gulp.dest( 'dist/' ) );
});

gulp.task('assets', function() {
  return gulp.src(['client/assets/**/*'])
    .pipe(gulp.dest('dist/assets/'));
});

gulp.task('compilewebpack', function() {
  var config = require('../../webpack.config.js');  
  if(argv.DEV) {
    config.devtool = 'source-map';
    console.log('DEV mode, loading source map for webpack');
  } else {
    console.log('PROD mode, source map for webpack will not be loaded');
  }
  return gulp.src('client/src/')
    .pipe( webpack( config ) )
    .pipe(gulp.dest('dist/'));
});
