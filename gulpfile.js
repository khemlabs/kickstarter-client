var gulp       = require('gulp');
var requireDir = require('require-dir');
var dir        = requireDir('lib/gulp_tasks/');

// ********** GULP CONFIGURATION **********

gulp.task('compile', [ 'compilewebpack', 'assets', 'env-js', 'html', 'index' ]);

gulp.task('watch-client', function() {
  console.log('Listening "client/**/*" for compile and restart ...');
  gulp.watch(['client/**/*'], ['compile']);
});

gulp.task('init', ['compile']);

gulp.task('init-dev', ['init', 'watch-client']);
